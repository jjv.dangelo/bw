use crate::ecs::Store;

use ngen::*;

pub trait System {
    fn run(&mut self, platform: &mut Platform<'_>, store: &mut Store, input: &Input, tick: &Tick);
}

pub struct Systems {
    systems: Vec<Box<dyn System>>,
}

impl Systems {
    pub const fn new() -> Self {
        Self {
            systems: Vec::new(),
        }
    }

    pub fn register<T: System + 'static>(&mut self, system: T) {
        self.systems.push(box system);
    }

    pub fn register_default<T: System + Default + 'static>(&mut self) {
        self.register(T::default())
    }

    pub fn run(&mut self, platform: &mut Platform<'_>, store: &mut Store, input: &Input, tick: &Tick) {
        for system in self.systems.iter_mut() {
            system.run(platform, store, input, tick);
        }
    }
}
