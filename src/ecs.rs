#![allow(dead_code)]

mod sys;

pub use sys::{System, Systems};

// TODO: Consider making Entity a trait so we can use u16, u32, u64, and even u128 if we want to
// for Entity ids. The biggest pain will be to propogate the trait throughout everything here. We
// could provide sensible defaults through `type Store = Store<u32>` or something, but that should
// be well thought out.
//
// We already moved the entity-specific constants into the Entity impl block, which will help us
// understand what the entity trait will need to expost for the rest of the module to work
// correctly.
//
// TODO: All of the calls to `new` in this module should be equivalent to `::with_capacity(0)`
// instead of pushing that onto the caller.

use std::{
    any::{Any, TypeId},
    borrow::{Borrow, BorrowMut},
    collections::HashMap,
    ops::{Deref, DerefMut},
    slice,
};

/// A 32-bit entity that carries both the [`Entity`]'s [`id`] along with its [`generation`].
///
/// # Notes
///
/// Entities are used in lieu of references and are, essentially, indexes into a `Vec`.
///
/// [`Entity`]: ./struct.Entity.html
/// [`id`]: ./struct.Entity.html#method.id
/// [`generation`]: ./struct.Entity.html#method.generation
#[derive(Clone, Copy, Default, Debug, Eq, Hash, PartialEq)]
pub struct Entity {
    value: u32,
}

impl Entity {
    const ENTITY_ID_MASK: u32 = 0xFFFFF;
    const ENTITY_ID_OFFSET: usize = 20;
    const NULL_ENTITY_ID: usize = Self::ENTITY_ID_MASK as usize;
    const MAX_GENERATION: u16 = 0xFFF;

    pub const NULL_ENTITY: Self = Self {
        value: Self::NULL_ENTITY_ID as u32,
    };

    /// The [`Entity`]'s `id` as a `usize`.
    ///
    /// [`Entity`]: ./struct.Entity.html
    pub fn id(self) -> usize {
        (self.value & Self::ENTITY_ID_MASK) as usize
    }

    /// The [`Entity`]'s generation.
    ///
    /// [`Entity`]: ./struct.Entity.html
    pub fn generation(self) -> u16 {
        (self.value >> Self::ENTITY_ID_OFFSET) as u16
    }

    fn new(value: usize, generation: u16) -> Self {
        assert!(value <= Self::NULL_ENTITY_ID);

        let generation = generation as u32;
        let value = (value as u32) | (generation << Self::ENTITY_ID_OFFSET);

        Self { value }
    }
}

impl From<usize> for Entity {
    fn from(value: usize) -> Self {
        assert!(value < Self::NULL_ENTITY_ID);
        Entity {
            value: value as u32,
        }
    }
}

/// A [`GenerationalAllocator`] that create monotonic-increasing entities and re-uses destroyed
/// entities by monotonically increasing their generation.
///
/// # Notes
///
/// An [`Entity`] is currently represented by a 32-bit unsigned integer. The 12 high-bits are
/// used for the generation while the lower 20 bits are used to track the entity. This means
/// that this system can currently track 2^20 entities, each of which can have a maximum
/// generation of 2^12.
///
/// The [`Entity`]'s generation will roll over to zero after surpassing the [`maximum generation`].
///
/// # Examples
///
/// ```
///# use lib::ecs::GenerationalAllocator;
/// let mut allocator = GenerationalAllocator::new();
/// let entity_1 = allocator.create();
/// allocator.destroy(entity_1);
///
/// let entity_2 = allocator.create();
/// assert_eq!(entity_1.id(), entity_2.id());
/// assert_eq!(entity_1.generation() + 1, entity_2.generation());
/// ```
///
/// [`GenerationalAllocator`]: ./struct.GenerationalAllocator.html
/// [`Entity`]: ./struct.Entity.html
/// [`maximum generation`]: ./constant.MAX_GENERATION.html
pub struct GenerationalAllocator {
    count: usize,
    entities: Vec<Entity>,
    next: usize,
}

impl Default for GenerationalAllocator {
    fn default() -> Self {
        Self::new()
    }
}

impl GenerationalAllocator {
    /// Creates a new [`GenerationalAllocator`] with a capacity of 64 entities.
    ///
    /// # Notes
    ///
    /// To avoid initial allocations, call [`GenerationalAllocator::with_capacity`] with `0` instead.
    ///
    /// [`GenerationalAllocator`]: ./struct.GenerationalAllocator.html
    /// [`GenerationalAllocator::with_capacity`]: ./struct.GenerationalAllocator.html#method.with_capacity
    pub fn new() -> Self {
        Self::with_capacity(64)
    }

    /// Creates a new [`GenerationalAllocator`] with the provided capacity.
    ///
    /// [`GenerationalAllocator`]: ./struct.GenerationalAllocator.html
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            count: 0,
            entities: Vec::with_capacity(capacity),
            next: Entity::NULL_ENTITY_ID,
        }
    }

    pub fn count(&self) -> usize {
        self.count
    }

    /// Creates a new [`Entity`].
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::GenerationalAllocator;
    /// let mut allocator = GenerationalAllocator::new();
    /// let entity = allocator.create();
    /// ```
    ///
    /// [`Entity`]: ./struct.Entity.html
    pub fn create(&mut self) -> Entity {
        self.count += 1;

        // Check to see if we have an available, previously destroyed, entity.
        if self.next == Entity::NULL_ENTITY_ID {
            let value = self.entities.len();
            let entity = value.into();
            self.entities.push(entity);
            entity
        } else {
            let next = self.next;
            let existing = &mut self.entities[next];
            let generation = existing.generation();

            self.next = existing.id();
            let new = Entity::new(next, generation);

            *existing = new;
            new
        }
    }

    /// Destroys an [`Entity`] and increments its generation by `1`.
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::GenerationalAllocator;
    /// let mut allocator = GenerationalAllocator::new();
    /// let entity = allocator.create();
    ///
    /// // Do something with `entity`.
    ///
    /// allocator.destroy(entity);
    /// ```
    ///
    /// [`Entity`]: ./struct.Entity.html
    pub fn destroy(&mut self, entity: Entity) {
        self.destroy_generation(entity, entity.generation() + 1);
    }

    /// Destroys an entity and sets the next generation value.
    ///
    /// # Notes
    ///
    /// Calling `destroy_generation` with an [`Entity`]'s generation incremented by one is the same
    /// as calling [`destroy`] on the entity.
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::GenerationalAllocator;
    /// let mut allocator = GenerationalAllocator::new();
    /// let entity = allocator.create();
    /// allocator.destroy_generation(entity, 42);
    ///
    /// let entity = allocator.create();
    /// assert_eq!(entity.generation(), 42);
    /// ```
    ///
    /// [`Entity`]: ./struct.Entity.html
    /// [`destroy`]: ./struct.GenerationalAllocator.html#method.destroy
    pub fn destroy_generation(&mut self, entity: Entity, generation: u16) {
        self.count -= 1;

        let deleted = entity.id();

        // Point to the next available ID to chain them along.
        self.entities[deleted] = Entity::new(self.next, generation);

        // Save this as the next available entity instead of allocating a new one.
        self.next = deleted;
    }

    /// Allows the caller to affirm the [`Entity`] has not been destroyed and
    /// that its generation is current.
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::GenerationalAllocator;
    /// let mut allocator = GenerationalAllocator::new();
    /// let entity = allocator.create();
    ///
    /// assert!(allocator.is_live(entity));
    ///
    /// allocator.destroy(entity);
    /// assert!(!allocator.is_live(entity));
    ///
    /// let entity_2 = allocator.create();
    /// assert!(!allocator.is_live(entity));
    /// assert_eq!(entity.id(), entity_2.id());
    /// ```
    ///
    /// [`Entity`]: ./struct.Entity.html
    pub fn is_live(&self, entity: Entity) -> bool {
        let entity_id = entity.id();
        let entity_generation = entity.generation();

        entity_id < self.entities.len()
            && self.entities[entity_id].id() == entity_id
            && self.entities[entity_id].generation() == entity_generation
    }
}

/// A [`SparseSet<>`] entry that maps an [`Entity`] to its value.
///
/// # Notes
///
/// [`Entity`] implements `std::borrow::Borrow`, `std::borrow::BorrowMut`, `std::ops::Deref`,
/// `std::ops::DerefMut`, `std::convert::AsRef`, and `std::conver::AsMut` as convenient access
/// to the underlying value.
///
/// # Examples
///
/// ```
///# use lib::ecs::{GenerationalAllocator, SparseSet};
///# let mut alloc = GenerationalAllocator::new();
///# let mut set = SparseSet::new();
///# let entity = alloc.create();
/// let add_one = |value: &mut i32| *value += 1;
///
/// set.set(entity, 38i32);
///
/// for mut entry in set.iter_mut() {
///     add_one(&mut entry);
///     add_one(*entry);
///     add_one(entry.as_mut());
///     add_one(entry.value);
///     assert_eq!(*entry, &42);
/// }
/// ```
///
/// [`SparseSet<>`]: ./struct.SparseSet.html
/// [`Entity`]: ./struct.Entity.html
pub struct Entry<T> {
    /// The [`Entity`].
    ///
    /// [`Entity`]: ./struct.Entity.html
    pub entity: Entity,

    /// The value associated with the [`Entity`].
    ///
    /// [`Entity`]: ./struct.Entity.html
    pub value: T,
}

impl<T> Borrow<T> for Entry<T> {
    fn borrow(&self) -> &T {
        &self.value
    }
}

impl<T> BorrowMut<T> for Entry<T> {
    fn borrow_mut(&mut self) -> &mut T {
        &mut self.value
    }
}

impl<T> Deref for Entry<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

impl<T> DerefMut for Entry<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.value
    }
}

impl<T> AsRef<T> for Entry<T> {
    fn as_ref(&self) -> &T {
        &self.value
    }
}

impl<T> AsMut<T> for Entry<T> {
    fn as_mut(&mut self) -> &mut T {
        &mut self.value
    }
}

/// A [`SparseSet`] that does not currently use paging to map a sparse set of inputs to a finite
/// set of values. This implementation uses two `Vec`s&mdash;a packed `Vec` and a sparse `Vec`.
/// Look-ups are equivalent to `&self.packed[self.sparse[entity]]`.
pub struct SparseSet<T> {
    packed: Vec<Entry<T>>,
    sparse: Vec<u32>,
}

impl<T> SparseSet<T> {
    pub fn new() -> Self {
        Self::with_capacity(32)
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            packed: Vec::new(),
            sparse: Vec::with_capacity(capacity),
        }
    }

    pub fn len(&self) -> usize {
        self.packed.len()
    }

    fn extend(&mut self, max_id: usize) {
        if max_id >= self.sparse.len() {
            let diff = max_id - self.sparse.len() + 1;
            let mut extended = vec![Entity::NULL_ENTITY_ID as u32; diff];
            self.sparse.append(&mut extended);
        }
    }

    pub fn set(&mut self, entity: Entity, component: T) -> Option<&mut T> {
        let id = entity.id();
        self.extend(entity.id());

        let packed_loc = &mut self.sparse[id];
        if *packed_loc == Entity::NULL_ENTITY_ID as u32 {
            *packed_loc = self.packed.len() as u32;
            self.packed.push(Entry {
                entity,
                value: component,
            });

            Some(&mut self.packed[*packed_loc as usize].value)
        } else {
            let entry = &mut self.packed[*packed_loc as usize];
            if entry.entity.generation() <= entity.generation() {
                entry.value = component;

                Some(&mut entry.value)
            } else {
                None
            }
        }
    }

    pub fn get(&self, entity: Entity) -> Option<&T> {
        let id = entity.id();
        if id >= self.sparse.len() {
            return None;
        }

        let packed_id = self.sparse[id] as usize;
        if packed_id == Entity::NULL_ENTITY_ID {
            return None;
        }

        let entry = &self.packed[packed_id];
        if entry.entity == entity {
            Some(&entry.value)
        } else {
            None
        }
    }

    pub fn get_mut(&mut self, entity: Entity) -> Option<&mut T> {
        let id = entity.id();
        if id >= self.sparse.len() {
            return None;
        }

        let packed_id = self.sparse[id];
        if packed_id == Entity::NULL_ENTITY_ID as u32 {
            return None;
        }

        let entry = &mut self.packed[packed_id as usize];
        if entry.entity == entity {
            Some(&mut entry.value)
        } else {
            None
        }
    }

    pub fn remove(&mut self, entity: Entity) -> Option<T> {
        // NOTE: We don't need to test packed_id == ENTITY_NULL_ID because ENTITY_NULL_ID should
        // never be in the packed array.
        let id = entity.id();
        let packed_id = self.sparse.get(id).cloned()? as usize;

        if packed_id > self.packed.len() {
            return None;
        }

        // Update the element's sparse location to point to the NULL_ENTITY_ID, allowing us to
        // short-circuit future look-ups.
        self.sparse
            .get_mut(id)
            .map(|e| *e = Entity::NULL_ENTITY_ID as u32);

        // Remove the target entry, swapping the last element.
        let existing = self.packed.swap_remove(packed_id);

        // If we swapped an element into this position, we need to update the sparse element to
        // point to the new (swapped) location.
        if let Some(swapped_entry) = self.packed.get(packed_id) {
            // swapped_entry_id points to the sparse location of the *swapped* element.
            let swapped_entry_id = swapped_entry.entity.id();

            if let Some(sparse_entry) = self.sparse.get_mut(swapped_entry_id) {
                // Change the sparse_entry to point to the new packed location where the original
                // element used to live.
                *sparse_entry = packed_id as u32;
            }
        }

        // And then return the existing value back to the caller in case they want to do something
        // with it.
        Some(existing.value)
    }

    pub fn iter(&self) -> SparseSetIter<'_, T> {
        SparseSetIter::new(&self.packed)
    }

    pub fn iter_mut(&mut self) -> SparseSetIterMut<'_, T> {
        SparseSetIterMut::new(&mut self.packed)
    }
}

pub struct SparseSetIter<'p, T> {
    iter: slice::Iter<'p, Entry<T>>,
}

impl<'p, T> SparseSetIter<'p, T> {
    fn new(packed: &'p Vec<Entry<T>>) -> Self {
        Self {
            iter: packed.iter(),
        }
    }
}

impl<'p, T> Iterator for SparseSetIter<'p, T> {
    type Item = &'p Entry<T>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }

    fn count(self) -> usize {
        self.iter.count()
    }

    fn last(self) -> Option<Self::Item> {
        self.iter.last()
    }

    fn nth(&mut self, n: usize) -> Option<Self::Item> {
        self.iter.nth(n)
    }
}

pub struct SparseSetIterMut<'p, T> {
    iter: slice::IterMut<'p, Entry<T>>,
}

impl<'p, T> SparseSetIterMut<'p, T> {
    fn new(packed: &'p mut Vec<Entry<T>>) -> Self {
        Self {
            iter: packed.iter_mut(),
        }
    }
}

impl<'p, T> Iterator for SparseSetIterMut<'p, T> {
    type Item = Entry<&'p mut T>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|e| Entry {
            entity: e.entity,
            value: &mut e.value,
        })
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }

    fn count(self) -> usize {
        self.iter.count()
    }

    fn last(self) -> Option<Self::Item> {
        self.iter.last().map(|e| Entry {
            entity: e.entity,
            value: &mut e.value,
        })
    }

    fn nth(&mut self, n: usize) -> Option<Self::Item> {
        self.iter.nth(n).map(|e| Entry {
            entity: e.entity,
            value: &mut e.value,
        })
    }
}

impl<T> IntoIterator for SparseSet<T> {
    type Item = Entry<T>;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.packed.into_iter()
    }
}

trait Set: Any {
    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;
    fn remove(&mut self, entity: Entity);
}

impl<T: 'static> Set for SparseSet<T> {
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }

    fn remove(&mut self, entity: Entity) {
        self.remove(entity);
    }
}

pub struct StoreBuilder {
    sets: HashMap<TypeId, Box<dyn Set>>,
}

impl StoreBuilder {
    fn new() -> Self {
        Self {
            sets: HashMap::new(),
        }
    }

    pub fn add_set<T: 'static>(mut self, capacity: usize) -> Self {
        self.sets.insert(
            TypeId::of::<T>(),
            box SparseSet::<T>::with_capacity(capacity),
        );

        self
    }

    pub fn build(self, entity_capacity: usize) -> Store {
        Store {
            allocator: GenerationalAllocator::with_capacity(entity_capacity),
            sets: self.sets,
        }
    }
}

/// Basic ECS `Store` that wraps a [`GenerationalAllocator` ] and [`SparseSet<>`]s to keep track of
/// entities and components.
///
/// [`SparseSet<>`]: ./struct.SparseSet.html
/// ['GenerationalAllocator`]: ./struct.GenerationalAllocator.html
///
/// # Examples
///
/// ```
///# use lib::ecs::Store;
/// let mut store = Store::new();
/// let entity = store.create_entity();
/// store.set(entity, 42u32);
/// assert_eq!(store.get::<u32>(entity), Some(&42));
/// ```
pub struct Store {
    /// The underlying [`GenerationalAllocator`] used to manage entities.
    ///
    /// [`GenerationalAllocator`]: ./struct.GenerationalAllocator.html
    allocator: GenerationalAllocator,

    /// Collection of [`SparseSet<>`]s for each component.
    ///
    /// [`SparseSet<>`]: ./struct.SparseSet.html
    sets: HashMap<TypeId, Box<dyn Set>>,
}

impl<'s> Store {
    /// Creates a [`StoreBuilder`] that helps with initializing component sets.
    ///
    /// [`StoreBuilder`]: ./struct.StoreBuilder.html
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::Store;
    /// struct Vec2 { x: f32, y: f32 };
    ///
    /// let store = Store::builder()
    ///     .add_set::<u32>(256)
    ///     .add_set::<Vec2>(256)
    ///     .build(256);
    /// ```
    pub fn builder() -> StoreBuilder {
        StoreBuilder::new()
    }

    /// Creates a `Store` with the [`GenerationalAllocator`]'s capacity set to 32.
    ///
    /// # Note
    /// Use [`Store::with_capacity`] set to 0 to avoid any allocations at creation.
    ///
    /// [`GenerationalAllocator`]: ./struct.GenerationalAllocator.html
    /// [`Store::with_capacity`]: ./struct.Store.html#method.with_capacity
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::Store;
    /// let mut store = Store::new();
    /// ```
    pub fn new() -> Self {
        Self::with_capacity(32)
    }

    /// Creates a `Store` with the [`GenerationalAllocator`]'s capacity set to the provided
    /// capacity.
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::Store;
    /// let mut store = Store::with_capacity(128);
    /// ```
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            allocator: GenerationalAllocator::with_capacity(capacity),
            sets: HashMap::new(),
        }
    }

    /// Creates a new entity with the underlying [`GenerationalAllocator`].
    ///
    /// [`GenerationalAllocator`]: ./struct.GenerationalAllocator.html
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::Store;
    /// let mut store = Store::new();
    /// let entity = store.create_entity();
    /// ```
    pub fn create_entity(&mut self) -> Entity {
        self.allocator.create()
    }

    /// Destroys the entity and removes it from all underlying component sets as well.
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::Store;
    /// let mut store = Store::new();
    ///
    /// // Create the entity
    /// let entity = store.create_entity();
    ///
    /// // Add a component to the entity.
    /// store.set(entity, 42u32);
    ///
    /// // Destroy the entity.
    /// store.destroy_entity(entity);
    ///
    /// assert_eq!(store.get::<u32>(entity), None);
    /// ```
    pub fn destroy_entity(&mut self, entity: Entity) {
        self.allocator.destroy(entity);

        for s in self.sets.values_mut() {
            s.remove(entity);
        }
    }

    pub fn entity_count(&self) -> usize {
        self.allocator.count()
    }

    pub fn is_live(&self, entity: Entity) -> bool {
        self.allocator.is_live(entity)
    }

    pub fn init_component_set<T: 'static>(&mut self, capacity: usize) -> bool {
        let type_id = TypeId::of::<T>();

        if self.sets.contains_key(&type_id) {
            false
        } else {
            self.sets
                .insert(type_id, box SparseSet::<T>::with_capacity(capacity));
            true
        }
    }

    pub fn components<T: 'static>(&'s self) -> Option<&'s SparseSet<T>> {
        self.sets
            .get(&TypeId::of::<T>())?
            .as_any()
            .downcast_ref::<SparseSet<T>>()
    }

    pub fn components_mut<T: 'static>(&'s mut self) -> Option<&'s mut SparseSet<T>> {
        self.sets
            .get_mut(&TypeId::of::<T>())?
            .as_any_mut()
            .downcast_mut::<SparseSet<T>>()
    }

    pub fn get<T: 'static>(&'s self, entity: Entity) -> Option<&'s T> {
        self.components::<T>().and_then(|s| s.get(entity))
    }

    pub fn get_mut<T: 'static>(&'s mut self, entity: Entity) -> Option<&'s mut T> {
        self.components_mut::<T>().and_then(|s| s.get_mut(entity))
    }

    pub fn set<T: 'static>(&mut self, entity: Entity, value: T) -> Option<&mut T> {
        self.sets
            .entry(TypeId::of::<T>())
            .or_insert_with(|| box SparseSet::<T>::new())
            .as_any_mut()
            .downcast_mut::<SparseSet<T>>()
            .and_then(|s| s.set(entity, value))
    }

    pub fn get_or_insert<T: 'static>(&mut self, entity: Entity, value: T) -> Option<&mut T> {
        if let None = self.get::<T>(entity) {
            self.set(entity, value);
        }

        self.get_mut(entity)
    }

    pub fn get_or_default<T: Default + 'static>(&mut self, entity: Entity) -> Option<&mut T> {
        self.get_or_insert(entity, T::default())
    }

    pub fn set_with<T: 'static, F: FnMut(&mut T)>(&mut self, entity: Entity, setter: F) {
        self.get_mut(entity).map(setter);
    }

    pub fn remove<T: 'static>(&mut self, entity: Entity) -> Option<T> {
        self.components_mut::<T>().and_then(|s| s.remove(entity))
    }

    /// Returns a [`ViewIter`]<'_, T> over all entities that contain the target components.
    ///
    /// # Notes
    ///
    /// `(T, U)` and `(T, U, V)` both implement [`View`]<'_> for ease of use.
    ///
    /// Use [`iter`] for single-component views instead of `view`;
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::Store;
    /// let mut store = Store::new();
    ///
    /// for i in 0..10i32 {
    ///     let entity = store.create_entity();
    ///     store.set(entity, i);
    ///     store.set(entity, i as i64);
    ///  }
    ///
    ///  for (entity, x, y) in store.view::<(i32, i64)>() {
    ///     assert_eq!(*x as i64, *y);
    ///  }
    ///  ```
    ///
    /// [`ViewIter`]: ./struct.ViewIter.html
    /// [`View`]: ./struct.View.html
    /// [`view`]: ./struct.Store.html#method.view
    /// [`iter`]: ./struct.Store.html#method.iter
    pub fn view<V: View<'s>>(&'s self) -> ViewIter<'s, V> {
        ViewIter::new(self)
    }

    /// Returns an iterator over all components of `T`.
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::Store;
    /// let mut store = Store::new();
    ///
    /// for i in 0..10u32 {
    ///     let entity = store.create_entity();
    ///     store.set(entity, i);
    /// }
    ///
    /// for (entity, i) in store.iter() {
    ///     assert_eq!(entity.id() as u32, *i);
    /// }
    /// ```
    pub fn iter<'v, T: 'static>(&'v self) -> ViewIter<'v, V<T>> {
        ViewIter::new(self)
    }

    /// Returns a [`ViewIterMut`]<'_, T> over all entities that contain the target components.
    ///
    /// # Notes
    ///
    /// `(T, U)` and `(T, U, V)` both implement [`ViewMut`]<'_> for ease of use.
    ///
    /// Use [`iter_mut`] for single-component views instead of `view_mut`;
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::Store;
    /// let mut store = Store::new();
    ///
    /// for i in 0..10i32 {
    ///     let entity = store.create_entity();
    ///     store.set(entity, i);
    ///     store.set(entity, i as i64);
    ///  }
    ///
    ///  for (entity, x, y) in store.view_mut::<(i32, i64)>() {
    ///     *x *= *y as i32;
    ///     assert_eq!(entity.id() as i32 * *y as i32, *x);
    ///  }
    ///  ```
    ///
    /// [`ViewIterMut`]: ./struct.ViewIterMut.html
    /// [`ViewMut`]: ./struct.ViewMut.html
    /// [`view_mut`]: ./struct.Store.html#method.view_mut
    /// [`iter_mut`]: ./struct.Store.html#method.iter_mut
    pub fn view_mut<V: ViewMut<'s>>(&'s mut self) -> ViewIterMut<'s, V> {
        ViewIterMut::new(self)
    }

    /// Returns a mutable iterator over all components of `T`.
    ///
    /// # Examples
    ///
    /// ```
    ///# use lib::ecs::Store;
    /// let mut store = Store::new();
    ///
    /// for i in 0..10u32 {
    ///     let entity = store.create_entity();
    ///     store.set(entity, i);
    /// }
    ///
    /// for (entity, i) in store.iter_mut() {
    ///     *i *= 2;
    ///     assert_eq!(entity.id() as u32 * 2, *i);
    /// }
    /// ```
    pub fn iter_mut<'v, T: 'static>(&'v mut self) -> ViewIterMut<'v, V<T>> {
        ViewIterMut::new(self)
    }
}

/// A trait used by [`View`]<'s>'s to keep track of their state between each iteration.
///
/// [`View`]: ./trait.View.html
pub trait ViewState<'s> {
    fn init(store: &'s Store) -> Self;
}

/// A trait to describe a a set of components that make up a particular view.
pub trait View<'s> {
    type Item;
    type State: ViewState<'s>;

    fn next(state: &mut Self::State) -> Option<Self::Item>;
}

/// A trait used by [`ViewMut`]<'s>'s to keep track of their state between each iteration.
///
/// [`View`]: ./trait.View.html
pub trait ViewStateMut<'s> {
    fn init(store: &'s mut Store) -> Self;
}

/// A trait to describe a a set of components that make up a particular mutable view.
pub trait ViewMut<'s> {
    type Item;
    type State: ViewStateMut<'s>;

    fn next(state: &mut Self::State) -> Option<Self::Item>;
}

pub struct ViewIter<'s, V: View<'s>> {
    state: V::State,
}

impl<'s, V: View<'s>> ViewIter<'s, V> {
    fn new(store: &'s Store) -> Self {
        let state = V::State::init(store);
        Self { state }
    }
}

impl<'s, V: View<'s>> Iterator for ViewIter<'s, V> {
    type Item = V::Item;

    fn next(&mut self) -> Option<Self::Item> {
        V::next(&mut self.state)
    }
}

pub struct ViewIterMut<'s, V: ViewMut<'s>> {
    state: V::State,
}

impl<'s, V: ViewMut<'s>> ViewIterMut<'s, V> {
    fn new(store: &'s mut Store) -> Self {
        let state = V::State::init(store);
        Self { state }
    }
}

impl<'s, V: ViewMut<'s>> Iterator for ViewIterMut<'s, V> {
    type Item = V::Item;

    fn next(&mut self) -> Option<Self::Item> {
        V::next(&mut self.state)
    }
}

pub enum View1State<'s, T> {
    T(slice::Iter<'s, Entry<T>>),
    NotFound,
}

impl<'s, T: 'static> ViewState<'s> for View1State<'s, T> {
    fn init(store: &'s Store) -> Self {
        match store.components::<T>() {
            Some(set) => Self::T(set.packed.iter()),
            None => Self::NotFound,
        }
    }
}

pub enum View2State<'s, T, U> {
    T(slice::Iter<'s, Entry<T>>, &'s SparseSet<U>),
    U(slice::Iter<'s, Entry<U>>, &'s SparseSet<T>),
    NotFound,
}

impl<'s, T: 'static, U: 'static> ViewState<'s> for View2State<'s, T, U> {
    fn init(store: &'s Store) -> Self {
        let t = store.components::<T>();
        let u = store.components::<U>();

        match (t, u) {
            (Some(t), Some(u)) => {
                if t.len() < u.len() {
                    Self::T(t.packed.iter(), u)
                } else {
                    Self::U(u.packed.iter(), t)
                }
            }

            _ => Self::NotFound,
        }
    }
}

pub enum View1StateMut<'s, T> {
    T(slice::IterMut<'s, Entry<T>>),
    NotFound,
}

impl<'s, T: 'static> ViewStateMut<'s> for View1StateMut<'s, T> {
    fn init(store: &'s mut Store) -> Self {
        match store.components_mut::<T>() {
            Some(t) => Self::T(t.packed.iter_mut()),
            None => Self::NotFound,
        }
    }
}

pub enum View2StateMut<'s, T, U> {
    T(slice::IterMut<'s, Entry<T>>, &'s mut SparseSet<U>),
    U(slice::IterMut<'s, Entry<U>>, &'s mut SparseSet<T>),
    NotFound,
}

impl<'s, T: 'static, U: 'static> ViewStateMut<'s> for View2StateMut<'s, T, U> {
    fn init(store: &'s mut Store) -> Self {
        // TODO: Find a way to remove the unsafe reborrows here.
        // SAFETY: The `unsafe` reborrow is safe because we have an unshared reference to
        // Store. Each underlying SpareSet can only be borrowed through Store, as well, so we
        // know that no other caller will have a reference to the `SparseSet`. As well, the
        // View2StateMut 's lifetime binds it to the lifetime of Store.
        // TODO: This is likely unsafe given we could potentially do `store.view<(_, u32, u32)>()`
        // which could return two exclusive references that are no longer exclusive. It may make
        // sense to check that `TypeId::of::<T>() != TypeId::of::<U>() at runtime`.
        let t = if let Some(t) = store.components_mut::<T>() {
            unsafe { &mut *(t as *mut SparseSet<T>) }
        } else {
            return Self::NotFound;
        };

        let u = if let Some(u) = store.components_mut::<U>() {
            unsafe { &mut *(u as *mut SparseSet<U>) }
        } else {
            return Self::NotFound;
        };

        if t.len() > u.len() {
            Self::T(t.packed.iter_mut(), u)
        } else {
            Self::U(u.packed.iter_mut(), t)
        }
    }
}

pub struct V<V>(std::marker::PhantomData<V>);

impl<'s, T: 'static> View<'s> for V<T> {
    type Item = (Entity, &'s T);
    type State = View1State<'s, T>;

    fn next(state: &mut Self::State) -> Option<Self::Item> {
        match state {
            Self::State::T(t) => t.next().map(|e| (e.entity, &e.value)),
            Self::State::NotFound => None,
        }
    }
}

impl<'s, T: 'static> ViewMut<'s> for V<T> {
    type Item = (Entity, &'s mut T);
    type State = View1StateMut<'s, T>;

    fn next(state: &mut Self::State) -> Option<Self::Item> {
        match state {
            Self::State::T(t) => t.next().map(|e| (e.entity, &mut e.value)),
            Self::State::NotFound => None,
        }
    }
}

impl<'s, T: 'static, U: 'static> View<'s> for (T, U) {
    type Item = (Entity, &'s T, &'s U);
    type State = View2State<'s, T, U>;

    fn next(state: &mut Self::State) -> Option<Self::Item> {
        match state {
            Self::State::T(t, u) => {
                while let Some(entry) = t.next() {
                    if let Some(u) = u.get(entry.entity) {
                        return Some((entry.entity, &entry.value, u));
                    }
                }

                None
            }

            Self::State::U(u, t) => {
                while let Some(entry) = u.next() {
                    if let Some(t) = t.get(entry.entity) {
                        return Some((entry.entity, t, &entry.value));
                    }
                }

                None
            }

            Self::State::NotFound => None,
        }
    }
}

impl<'s, T: 'static> ViewMut<'s> for [T; 0] {
    type Item = (Entity, &'s mut T);
    type State = View1StateMut<'s, T>;

    fn next(state: &mut Self::State) -> Option<Self::Item> {
        match state {
            Self::State::T(t) => t.next().map(|e| (e.entity, &mut e.value)),
            Self::State::NotFound => None,
        }
    }
}

impl<'s, T: 'static, U: 'static> ViewMut<'s> for (T, U) {
    type Item = (Entity, &'s mut T, &'s mut U);
    type State = View2StateMut<'s, T, U>;

    fn next(state: &mut Self::State) -> Option<Self::Item> {
        match state {
            Self::State::T(t, u) => {
                while let Some(entry) = t.next() {
                    if let Some(u) = u.get_mut(entry.entity) {
                        let u = unsafe { &mut *(u as *mut U) };
                        return Some((entry.entity, &mut entry.value, u));
                    }
                }

                None
            }

            Self::State::U(u, t) => {
                while let Some(entry) = u.next() {
                    if let Some(t) = t.get_mut(entry.entity) {
                        let t = unsafe { &mut *(t as *mut T) };
                        return Some((entry.entity, t, &mut entry.value));
                    }
                }

                None
            }

            Self::State::NotFound => None,
        }
    }
}

pub enum View3State<'s, T, U, V> {
    T(
        slice::Iter<'s, Entry<T>>,
        &'s SparseSet<U>,
        &'s SparseSet<V>,
    ),
    U(
        slice::Iter<'s, Entry<U>>,
        &'s SparseSet<T>,
        &'s SparseSet<V>,
    ),
    V(
        slice::Iter<'s, Entry<V>>,
        &'s SparseSet<T>,
        &'s SparseSet<U>,
    ),
    NotFound,
}

impl<'s, T: 'static, U: 'static, V: 'static> ViewState<'s> for View3State<'s, T, U, V> {
    fn init(store: &'s Store) -> Self {
        let t = store.components::<T>();
        let u = store.components::<U>();
        let v = store.components::<V>();

        match (t, u, v) {
            (Some(t), Some(u), Some(v)) => {
                if t.len() < u.len() {
                    if t.len() < v.len() {
                        Self::T(t.packed.iter(), u, v)
                    } else {
                        Self::V(v.packed.iter(), t, u)
                    }
                } else if u.len() < v.len() {
                    Self::U(u.packed.iter(), t, v)
                } else {
                    Self::V(v.packed.iter(), t, u)
                }
            }

            _ => Self::NotFound,
        }
    }
}

pub enum View3StateMut<'s, T, U, V> {
    T(
        slice::IterMut<'s, Entry<T>>,
        &'s mut SparseSet<U>,
        &'s mut SparseSet<V>,
    ),
    U(
        slice::IterMut<'s, Entry<U>>,
        &'s mut SparseSet<T>,
        &'s mut SparseSet<V>,
    ),
    V(
        slice::IterMut<'s, Entry<V>>,
        &'s mut SparseSet<T>,
        &'s mut SparseSet<U>,
    ),
    NotFound,
}

impl<'s, T: 'static, U: 'static, V: 'static> ViewStateMut<'s> for View3StateMut<'s, T, U, V> {
    fn init(store: &'s mut Store) -> Self {
        let t = if let Some(t) = store.components_mut::<T>() {
            unsafe { &mut *(t as *mut SparseSet<T>) }
        } else {
            return Self::NotFound;
        };

        let u = if let Some(u) = store.components_mut::<U>() {
            unsafe { &mut *(u as *mut SparseSet<U>) }
        } else {
            return Self::NotFound;
        };

        let v = if let Some(v) = store.components_mut::<V>() {
            unsafe { &mut *(v as *mut SparseSet<V>) }
        } else {
            return Self::NotFound;
        };

        if t.len() < u.len() {
            if t.len() < v.len() {
                Self::T(t.packed.iter_mut(), u, v)
            } else {
                Self::V(v.packed.iter_mut(), t, u)
            }
        } else if u.len() < v.len() {
            Self::U(u.packed.iter_mut(), t, v)
        } else {
            Self::V(v.packed.iter_mut(), t, u)
        }
    }
}

impl<'s, T: 'static, U: 'static, V: 'static> View<'s> for (T, U, V) {
    type Item = (Entity, &'s T, &'s U, &'s V);
    type State = View3State<'s, T, U, V>;

    fn next(state: &mut Self::State) -> Option<Self::Item> {
        match state {
            Self::State::T(t, u, v) => {
                while let Some(entry) = t.next() {
                    if let Some(u) = u.get(entry.entity) {
                        if let Some(v) = v.get(entry.entity) {
                            return Some((entry.entity, &entry.value, u, v));
                        }
                    }
                }

                None
            }

            Self::State::U(u, t, v) => {
                while let Some(entry) = u.next() {
                    if let Some(t) = t.get(entry.entity) {
                        if let Some(v) = v.get(entry.entity) {
                            return Some((entry.entity, t, &entry.value, v));
                        }
                    }
                }

                None
            }

            Self::State::V(v, t, u) => {
                while let Some(entry) = v.next() {
                    if let Some(t) = t.get(entry.entity) {
                        if let Some(u) = u.get(entry.entity) {
                            return Some((entry.entity, t, u, &entry.value));
                        }
                    }
                }

                None
            }

            Self::State::NotFound => None,
        }
    }
}

impl<'s, T: 'static, U: 'static, V: 'static> ViewMut<'s> for (T, U, V) {
    type Item = (Entity, &'s mut T, &'s mut U, &'s mut V);
    type State = View3StateMut<'s, T, U, V>;

    fn next(state: &mut Self::State) -> Option<Self::Item> {
        match state {
            Self::State::T(t, u, v) => {
                while let Some(entry) = t.next() {
                    if let Some(u) = u.get_mut(entry.entity) {
                        if let Some(v) = v.get_mut(entry.entity) {
                            let u = unsafe { &mut *(u as *mut U) };
                            let v = unsafe { &mut *(v as *mut V) };

                            return Some((entry.entity, &mut entry.value, u, v));
                        }
                    }
                }

                None
            }

            Self::State::U(u, t, v) => {
                while let Some(entry) = u.next() {
                    if let Some(t) = t.get_mut(entry.entity) {
                        if let Some(v) = v.get_mut(entry.entity) {
                            let t = unsafe { &mut *(t as *mut T) };
                            let v = unsafe { &mut *(v as *mut V) };

                            return Some((entry.entity, t, &mut entry.value, v));
                        }
                    }
                }

                None
            }

            Self::State::V(v, t, u) => {
                while let Some(entry) = v.next() {
                    if let Some(t) = t.get_mut(entry.entity) {
                        if let Some(u) = u.get_mut(entry.entity) {
                            let t = unsafe { &mut *(t as *mut T) };
                            let u = unsafe { &mut *(u as *mut U) };

                            return Some((entry.entity, t, u, &mut entry.value));
                        }
                    }
                }

                None
            }

            Self::State::NotFound => None,
        }
    }
}

#[cfg(test)]
mod generational_allocator_tests {
    use super::*;

    #[test]
    fn can_create_a_default_allocator() {
        let _allocator = GenerationalAllocator::default();
        let _allocator = GenerationalAllocator::new();
    }

    #[test]
    fn can_create_an_allocator_with_a_given_capacity() {
        let allocator = GenerationalAllocator::with_capacity(1024);
        assert_eq!(allocator.entities.capacity(), 1024);
        assert_eq!(allocator.next, Entity::NULL_ENTITY_ID);
    }

    #[test]
    fn can_create_an_entity() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();

        assert_eq!(entity.id(), 0);
        assert_eq!(entity.generation(), 0);
    }

    #[test]
    fn can_destroy_an_entity() {
        let mut allocator = GenerationalAllocator::new();
        assert_eq!(allocator.next, Entity::NULL_ENTITY_ID);

        let entity = allocator.create();
        allocator.destroy(entity);

        assert_eq!(allocator.next, entity.id());
    }

    #[test]
    fn can_reuse_entities_after_they_are_destroyed() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();
        assert_eq!(entity.generation(), 0);

        allocator.destroy(entity);
        assert_eq!(allocator.next, entity.id());

        let new_entity = allocator.create();
        assert_eq!(allocator.next, Entity::NULL_ENTITY_ID);
        assert_eq!(new_entity.id(), 0);
        assert_eq!(new_entity.generation(), 1);
    }

    #[test]
    fn can_allocate_multiple_entities() {
        let mut allocator = GenerationalAllocator::new();

        for _ in 0..15 {
            allocator.create();
        }

        let entity = allocator.create();

        for _ in 0..5 {
            allocator.create();
        }

        allocator.destroy(entity);
        assert_eq!(allocator.next, entity.id());

        let new_entity = allocator.create();
        assert_eq!(new_entity.generation(), 1);
    }

    #[test]
    fn generation_uses_rolling_four_bits() {
        let mut allocator = GenerationalAllocator::new();

        for _ in 0..Entity::MAX_GENERATION {
            let entity = allocator.create();
            allocator.destroy(entity);
        }

        let entity = allocator.create();
        assert_eq!(entity.generation(), Entity::MAX_GENERATION);

        allocator.destroy(entity);

        let entity = allocator.create();
        assert_eq!(entity.generation(), 0);
    }

    #[test]
    fn a_new_entity_is_also_live() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();
        assert!(allocator.is_live(entity));
    }

    #[test]
    fn a_destroyed_entity_is_not_live() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();
        allocator.destroy(entity);
        assert!(!allocator.is_live(entity));
    }

    #[test]
    fn an_out_of_range_id_is_not_live() {
        let allocator = GenerationalAllocator::new();
        let entity = Entity::new(1024, 0);
        assert!(!allocator.is_live(entity));
    }
}

#[cfg(test)]
mod sparse_set_tests {
    use super::*;

    #[test]
    fn can_create_a_sparse_set() {
        let _sparse_set = SparseSet::<u32>::new();
    }

    #[test]
    fn can_insert_a_get_value() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();

        let value = 32;
        let mut array = SparseSet::new();
        array.set(entity, value);

        let found = array.get(entity);
        assert_eq!(found, Some(&value));
    }

    #[test]
    fn advancing_entity_generation_returns_none() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();

        let value = 32;
        let mut array = SparseSet::new();
        array.set(entity, value);

        allocator.destroy(entity);
        let new_entity = allocator.create();
        assert_eq!(entity.id(), new_entity.id());

        let found = array.get(new_entity);
        assert_eq!(found, None);
    }

    #[test]
    fn none_returned_when_nothing_added_for_entity() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();
        let array = SparseSet::<u32>::new();
        let found = array.get(entity);
        assert_eq!(found, None);
    }

    #[test]
    fn can_update_a_value() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();

        let mut value = 32;
        let mut array = SparseSet::new();
        array.set(entity, value);

        let found = array.get_mut(entity);
        assert_eq!(found, Some(&mut value));
        found.map(|v| *v = 42);

        let value = array.get(entity);
        assert_eq!(value, Some(&42));
    }

    #[test]
    fn can_remove_a_value() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();

        let value = 32;
        let mut array = SparseSet::new();
        array.set(entity, value);

        let found = array.remove(entity);
        assert_eq!(found, Some(value));

        let second_look = array.get(entity);
        assert_eq!(second_look, None);
    }

    #[test]
    fn return_none_if_the_entity_does_not_exist_in_the_set() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();

        let mut array = SparseSet::<u32>::new();
        let found = array.remove(entity);
        assert_eq!(found, None);
    }

    #[test]
    fn can_iterate_values() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();
        let entity_2 = allocator.create();
        let entity_3 = allocator.create();

        let mut set = SparseSet::new();
        set.set(entity, 0);
        set.set(entity_2, 1);
        set.set(entity_3, 2);

        let mut count = 0;
        for e in set.iter() {
            count += 1;

            if e.entity == entity {
                assert_eq!(e.value, 0);
            } else if e.entity == entity_2 {
                assert_eq!(e.value, 1);
            } else if e.entity == entity_3 {
                assert_eq!(e.value, 2);
            } else {
                unreachable!("Invalid entity found.");
            }
        }

        assert_eq!(count, 3);
    }

    #[test]
    fn can_iterate_mut_values() {
        let mut allocator = GenerationalAllocator::new();
        let entity = allocator.create();
        let entity_2 = allocator.create();
        let entity_3 = allocator.create();

        let mut set = SparseSet::new();
        set.set(entity, 0);
        set.set(entity_2, 1);
        set.set(entity_3, 2);

        let mut count = 0;
        for e in set.iter_mut() {
            count += 1;
            *e.value *= 2;

            if e.entity == entity {
                assert_eq!(e.value, &mut 0);
            } else if e.entity == entity_2 {
                assert_eq!(e.value, &mut 2);
            } else if e.entity == entity_3 {
                assert_eq!(e.value, &mut 4);
            } else {
                unreachable!("Invalid entity found.");
            }
        }

        assert_eq!(count, 3);
    }
}

#[cfg(test)]
mod store_tests {
    use super::*;

    #[test]
    fn can_create_a_store() {
        let _store = Store::new();
    }

    #[test]
    fn can_create_and_destroy_an_entity() {
        let mut store = Store::new();
        let entity = store.create_entity();
        store.destroy_entity(entity);
        let entity_2 = store.create_entity();
        assert_eq!(entity.id(), entity_2.id());
        assert_eq!(entity.generation() + 1, entity_2.generation());
    }

    #[test]
    fn can_init_a_component_set_to_set_initial_capacity() {
        let mut store = Store::new();
        store.init_component_set::<u32>(1024);
        let set = store
            .components::<u32>()
            .expect("Did not create a SparseSet.");
        assert_eq!(set.sparse.capacity(), 1024);
    }

    #[test]
    fn can_add_component_directly_to_sparse_set() {
        let mut store = Store::new();
        assert!(store.init_component_set::<u32>(1));

        let entity = store.create_entity();
        let set = store
            .components_mut::<u32>()
            .expect("Did not create a SparseSet.");
        set.set(entity, 42);
        assert_eq!(set.get(entity), Some(&42));
    }

    #[test]
    fn can_add_and_retrieve_component_for_entity() {
        let mut store = Store::new();
        let entity = store.create_entity();

        store.set(entity, 42);
        assert_eq!(store.get(entity), Some(&42));

        let val = store.get_mut(entity).expect("Could not get mutable value.");
        *val = 7;

        assert_eq!(store.get(entity), Some(&7));
    }

    #[test]
    fn can_get_a_view_from_a_two_tuple() {
        let mut store = Store::new();
        let mut targets = vec![];

        for i in 0..32 {
            let entity = store.create_entity();

            store.set(entity, i);
            if i & 1 == 1 {
                store.set(entity, i as f32);
                targets.push(entity);
            }
        }

        // TODO: Remove things from `targets` as we iterate the view and assert
        // `targets` is empty when we're done.
        for (entity, t, u) in store.view::<(_, _)>() {
            assert_eq!(entity.id() as u32, *t);
            assert_eq!(entity.id() as f32, *u);
            assert!(targets.contains(&entity));
        }
    }

    #[test]
    fn can_get_a_mutable_view_from_a_two_tuple() {
        const TOTAL_ENTITIES: usize = 32;

        let mut store = Store::new();

        for i in 0..TOTAL_ENTITIES {
            let entity = store.create_entity();

            store.set(entity, i as u32);
            store.set(entity, i as f32);
        }

        let mut count = 0;
        for (entity, t) in store.iter() {
            count += 1;
            assert_eq!(entity.id() as u32, *t);
        }
        assert_eq!(count, TOTAL_ENTITIES);

        let mut count = 0;
        for (entity, t, u) in store.view_mut::<(_, _)>() {
            count += 1;

            assert_eq!(entity.id() as u32, *t);
            assert_eq!(entity.id() as f32, *u);
            *t += *u as u32;
            *u *= 3.;
            *u += 1.;
        }
        assert_eq!(count, TOTAL_ENTITIES);

        let mut count = 0;
        for entry in store.components().unwrap().iter() {
            count += 1;
            assert_eq!((entry.entity.id() * 2) as u32, entry.value);
        }
        assert_eq!(count, TOTAL_ENTITIES);

        let mut count = 0;
        for entry in store.components().unwrap().iter() {
            count += 1;
            assert_eq!((entry.entity.id() * 3 + 1) as f32, entry.value);
        }
        assert_eq!(count, TOTAL_ENTITIES);
    }

    #[test]
    fn destroying_an_entity_removes_it_from_all_sets() {
        let mut store = Store::new();
        let entity = store.create_entity();

        store.set::<u32>(entity, 42);
        store.set::<f32>(entity, 42.);

        assert_eq!(store.get::<u32>(entity), Some(&42));
        assert_eq!(store.get::<f32>(entity), Some(&42.));

        store.destroy_entity(entity);

        assert_eq!(store.get::<u32>(entity), None);
        assert_eq!(store.get::<f32>(entity), None);
    }

    #[test]
    fn this_should_not_happen() {
        let mut store = Store::new();
        let entity = store.create_entity();

        store.set::<u32>(entity, 42);

        // This shows we don't have exclusive references to the same underlying memory. Not good and
        // potentially harmful if the references are handed down through two separate code flows
        // that rely on the borrow checker being correct.
        for (_e, x, y) in store.view_mut::<(u32, u32)>() {
            assert_eq!(x, y);
            *x = 7;
            assert_eq!(x, y);
        }
    }
}
