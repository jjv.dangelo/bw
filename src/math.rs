pub use cgmath::prelude::*;

pub use ngen::math::{Vec2, Vec4};

#[derive(Clone, Copy)]
pub struct Ray {
    pub origin: Vec2,
    pub dir: Vec2,
    pub inv_dir: Vec2,
}

impl Ray {
    pub fn new(origin: Vec2, dir: Vec2) -> Self {
        Self {
            origin,
            dir,
            inv_dir: 1. / dir,
        }
    }
}
