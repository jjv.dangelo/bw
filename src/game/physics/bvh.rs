use crate::math::{Ray, Vec2};

#[derive(Copy, Clone, Debug)]
pub struct AABB {
    min: Vec2,
    max: Vec2,
}

impl AABB {
    pub fn new<V: Into<Vec2>>(min: V, max: V) -> Self {
        let min = min.into();
        let max = max.into();

        Self { min, max }
    }

    pub fn from_pos(pos: Vec2, dim: Vec2) -> Self {
        let half_dim = dim * 0.5;
        Self::new(pos - half_dim, pos + half_dim)
    }

    pub fn area(&self) -> f32 {
        let Vec2 { x, y } = self.max - self.min;
        x * y
    }

    pub fn overlaps(&self, other: &Self) -> bool {
        let dim = other.max - other.min;
        let pos = (other.max + other.min) * 0.5;

        let expanded = *self + dim;
        expanded.min.x <= pos.x
            && expanded.min.y <= pos.y
            && expanded.max.x >= pos.x
            && expanded.max.y >= pos.y
    }

    fn intersects(&self, ray: &Ray) -> Option<f32> {
        let Self { min, max } = self;
        let Ray {
            origin, inv_dir, ..
        } = ray;

        let mut t1 = (min.x - origin.x) * inv_dir.x;
        let mut t2 = (max.x - origin.x) * inv_dir.x;

        let mut t_min = f32::min(t1, t2);
        let mut t_max = f32::max(t1, t2);

        t1 = (min.y - origin.y) * inv_dir.y;
        t2 = (max.y - origin.y) * inv_dir.y;

        t_min = f32::max(t_min, f32::min(f32::min(t1, t2), t_max));
        t_max = f32::min(t_max, f32::max(f32::max(t1, t2), t_min));

        if t_max > f32::max(t_min, 0.) {
            Some(t_min)
        } else {
            None
        }
    }
}

impl std::ops::Add<AABB> for AABB {
    type Output = AABB;

    fn add(self, Self { min, max }: Self) -> Self::Output {
        use std::f32;

        let min = Vec2::new(f32::min(self.min.x, min.x), f32::min(self.min.y, min.y));
        let max = Vec2::new(f32::max(self.max.x, max.x), f32::max(self.max.y, max.y));

        Self::new(min, max)
    }
}

impl std::ops::Add<&AABB> for AABB {
    type Output = AABB;

    fn add(self, Self { min, max }: &Self) -> Self::Output {
        use std::f32;

        let min = Vec2::new(f32::min(self.min.x, min.x), f32::min(self.min.y, min.y));
        let max = Vec2::new(f32::max(self.max.x, max.x), f32::max(self.max.y, max.y));

        Self::new(min, max)
    }
}

impl std::ops::Add<AABB> for &AABB {
    type Output = AABB;

    fn add(self, AABB { min, max }: AABB) -> Self::Output {
        use std::f32;

        let min = Vec2::new(f32::min(self.min.x, min.x), f32::min(self.min.y, min.y));
        let max = Vec2::new(f32::max(self.max.x, max.x), f32::max(self.max.y, max.y));

        AABB::new(min, max)
    }
}

impl<'a> std::ops::Add<&'a AABB> for &'a AABB {
    type Output = AABB;

    fn add(self, AABB { min, max }: &'a AABB) -> Self::Output {
        use std::f32;

        let min = Vec2::new(f32::min(self.min.x, min.x), f32::min(self.min.y, min.y));
        let max = Vec2::new(f32::max(self.max.x, max.x), f32::max(self.max.y, max.y));

        AABB::new(min, max)
    }
}

impl<'a> std::ops::Add<&'a mut AABB> for &'a mut AABB {
    type Output = AABB;

    fn add(self, AABB { min, max }: &'a mut AABB) -> Self::Output {
        use std::f32;

        let min = Vec2::new(f32::min(self.min.x, min.x), f32::min(self.min.y, min.y));

        let max = Vec2::new(f32::max(self.max.x, max.x), f32::max(self.max.y, max.y));

        AABB::new(min, max)
    }
}

impl<'a> std::ops::Add<&'a AABB> for &'a mut AABB {
    type Output = AABB;

    fn add(self, AABB { min, max }: &'a AABB) -> Self::Output {
        use std::f32;

        let min = Vec2::new(f32::min(self.min.x, min.x), f32::min(self.min.y, min.y));
        let max = Vec2::new(f32::max(self.max.x, max.x), f32::max(self.max.y, max.y));

        AABB::new(min, max)
    }
}

impl<'a> std::ops::Add<&'a mut AABB> for &'a AABB {
    type Output = AABB;

    fn add(self, AABB { min, max }: &'a mut AABB) -> Self::Output {
        use std::f32;

        let min = Vec2::new(f32::min(self.min.x, min.x), f32::min(self.min.y, min.y));

        let max = Vec2::new(f32::max(self.max.x, max.x), f32::max(self.max.y, max.y));

        AABB::new(min, max)
    }
}

impl std::ops::Add<Vec2> for AABB {
    type Output = Self;

    fn add(self, dim: Vec2) -> Self::Output {
        let half_dim = dim * 0.5;
        AABB::new(self.min - half_dim, self.max + half_dim)
    }
}

enum Node<T> {
    Node {
        parent: Option<usize>,
        aabb: AABB,
        left: usize,
        right: usize,
    },

    Leaf {
        parent: Option<usize>,
        aabb: AABB,
        value: T,
    },
}

impl<T> Node<T> {
    fn aabb(&self) -> &AABB {
        match self {
            Self::Node { aabb, .. } | Self::Leaf { aabb, .. } => aabb,
        }
    }

    fn compute_cost(&self, aabb: &AABB, cost: f32) -> f32 {
        match self {
            Self::Node { aabb: bb, .. } => {
                let old_area = bb.area();
                let bb = bb + aabb;
                let new_area = bb.area();
                new_area - old_area + cost
            }

            Self::Leaf { aabb: bb, .. } => {
                let bb = bb + aabb;
                bb.area() + cost
            }
        }
    }

    fn parent(&self) -> Option<usize> {
        match self {
            Self::Node { parent, .. } | Self::Leaf { parent, .. } => *parent,
        }
    }

    fn set_parent(&mut self, parent: usize) {
        match self {
            Self::Node { parent: p, .. } | Self::Leaf { parent: p, .. } => *p = Some(parent),
        }
    }

    fn update_child(&mut self, old_child: usize, new_child: usize) {
        match self {
            Self::Node { left, .. } if *left == old_child => {
                *left = new_child;
            }

            Self::Node { right, .. } if *right == old_child => {
                *right = new_child;
            }

            Self::Node { .. } => panic!(
                "Cannot update children. {} is not an existing child.",
                old_child
            ),
            Self::Leaf { .. } => panic!("Cannot update children on a leaf node."),
        }
    }

    fn get_children(&self) -> (usize, usize) {
        match self {
            Self::Node { left, right, .. } => (*left, *right),
            Self::Leaf { .. } => panic!("Cannot get children from a leaf node."),
        }
    }

    fn set_aabb(&mut self, bb: AABB) {
        match self {
            Self::Node { aabb, .. } => *aabb = bb,
            Self::Leaf { aabb, .. } => *aabb = bb,
        }
    }
}

pub struct BVH<T> {
    nodes: Vec<Node<T>>,
    root: Option<usize>,
}

impl<T> BVH<T> {
    pub fn new() -> Self {
        Self {
            nodes: Vec::new(),
            root: None,
        }
    }

    pub fn insert(&mut self, aabb: AABB, value: T) {
        let sibling_id = match self.root {
            None => {
                let node_id = self.create_leaf(aabb, value);
                self.root = Some(node_id);

                return;
            }

            Some(root_id) => self.find_sibling(root_id, &aabb),
        };

        let old_parent = self.nodes[sibling_id].parent();
        let parent_aabb = self.nodes[sibling_id].aabb() + &aabb;

        let new_node_id = self.create_leaf(aabb, value);
        let new_parent_id = self.create_node(parent_aabb, old_parent, sibling_id, new_node_id);

        self.nodes[new_node_id].set_parent(new_parent_id);
        self.nodes[sibling_id].set_parent(new_parent_id);

        match old_parent {
            // The sibling didn't have a parent, so it was the root.
            None => self.root = Some(new_parent_id),

            // The sibling had a parent that needs to point to the new parent.
            Some(old_parent) => self.nodes[old_parent].update_child(sibling_id, new_parent_id),
        }

        self.fix_aabbs(new_parent_id);
    }

    fn create_leaf(&mut self, aabb: AABB, value: T) -> usize {
        let node_id = self.nodes.len();
        self.nodes.push(Node::Leaf {
            parent: None,
            aabb,
            value,
        });

        node_id
    }

    fn create_node(
        &mut self,
        aabb: AABB,
        parent: Option<usize>,
        left: usize,
        right: usize,
    ) -> usize {
        let id = self.nodes.len();
        self.nodes.push(Node::Node {
            aabb,
            parent,
            left,
            right,
        });

        id
    }

    fn find_sibling(&self, mut current: usize, new_aabb: &AABB) -> usize {
        loop {
            match &self.nodes[current] {
                Node::Leaf { .. } => break current,

                Node::Node {
                    aabb, left, right, ..
                } => {
                    let current_area = aabb.area();
                    let combined = aabb + new_aabb;
                    let combined_area = combined.area();
                    let cost = 2. * combined_area;
                    let inheritance_cost = 2. * (combined_area - current_area);

                    let l_child_cost = self.nodes[*left].compute_cost(new_aabb, inheritance_cost);

                    let r_child_cost = self.nodes[*right].compute_cost(new_aabb, inheritance_cost);

                    if cost < l_child_cost && cost < r_child_cost {
                        break current;
                    } else if l_child_cost < r_child_cost {
                        current = *left;
                    } else {
                        current = *right;
                    }
                }
            }
        }
    }

    fn fix_aabbs(&mut self, mut next: usize) {
        loop {
            let (left, right) = self.nodes[next].get_children();
            let aabb = self.calc_aabb(left, right);

            let node = &mut self.nodes[next];
            node.set_aabb(aabb);

            match node.parent() {
                Some(parent) => next = parent,
                None => break,
            }
        }
    }

    fn calc_aabb(&self, left: usize, right: usize) -> AABB {
        let left_aabb = self.nodes[left].aabb();
        let right_aabb = self.nodes[right].aabb();
        left_aabb + right_aabb
    }

    pub fn cast_ray(&self, ray: Ray, expand: Option<Vec2>) -> RayIter<'_, T> {
        RayIter::new(self, ray, expand)
    }

    pub fn cast_aabb(&self, aabb: AABB) -> AABBIter<'_, T> {
        AABBIter::new(self, aabb)
    }
}

pub struct AABBIter<'b, T> {
    bvh: &'b BVH<T>,
    aabb: AABB,
    nodes: Vec<usize>,
}

impl<'b, T> AABBIter<'b, T> {
    fn new(bvh: &'b BVH<T>, aabb: AABB) -> Self {
        let mut nodes = Vec::new();
        if let Some(root) = bvh.root {
            nodes.push(root);
        }

        Self { bvh, aabb, nodes }
    }
}

impl<'b, T> Iterator for AABBIter<'b, T> {
    type Item = &'b T;

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(node_id) = self.nodes.pop() {
            match &self.bvh.nodes[node_id] {
                Node::Node {
                    aabb, left, right, ..
                } => {
                    if self.aabb.overlaps(aabb) {
                        self.nodes.push(*left);
                        self.nodes.push(*right);
                    }
                }

                Node::Leaf { value, .. } => return Some(value),
            }
        }

        return None;
    }
}

pub struct RayIter<'b, T> {
    bvh: &'b BVH<T>,
    ray: Ray,
    expand: Vec2,
    nodes: Vec<usize>,
}

impl<'b, T> RayIter<'b, T> {
    fn new(bvh: &'b BVH<T>, ray: Ray, expand: Option<Vec2>) -> Self {
        let expand = expand.unwrap_or(Vec2::new(0., 0.));

        let mut nodes = Vec::new();
        if let Some(root) = bvh.root {
            nodes.push(root);
        }

        Self {
            bvh,
            ray,
            expand,
            nodes,
        }
    }
}

impl<'b, T: 'static> Iterator for RayIter<'b, T> {
    type Item = (&'b T, f32);

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(node_id) = self.nodes.pop() {
            match &self.bvh.nodes[node_id] {
                Node::Node {
                    aabb, left, right, ..
                } => {
                    let expanded = *aabb + self.expand;
                    if let Some(_) = expanded.intersects(&self.ray) {
                        self.nodes.push(*left);
                        self.nodes.push(*right);
                    }
                }

                Node::Leaf { aabb, value, .. } => {
                    let expanded = *aabb + self.expand;
                    if let Some(t) = expanded.intersects(&self.ray) {
                        let value = unsafe { &*(value as *const T) };
                        return Some((value, t));
                    }
                }
            }
        }

        return None;
    }
}

impl<T> std::iter::FromIterator<(AABB, T)> for BVH<T> {
    fn from_iter<I: IntoIterator<Item = (AABB, T)>>(iter: I) -> Self {
        // TODO: Since we have the entire collection, we can likely do a bottom-up build of the
        // BVH instead of adding things one at a time. We can convert each item in the iterator
        // into an Entry::Leaf and push them into `nodes`. Then we can build things up from there.
        let mut bvh = Self::new();

        for (aabb, t) in iter {
            bvh.insert(aabb, t);
        }

        bvh
    }
}

#[cfg(test)]
mod aabb_tests {
    use super::*;

    #[test]
    fn area_returns_correct_result() {
        let min = Vec2::new(-5., -5.);
        let max = Vec2::new(5., 5.);
        let bb = AABB::new(min, max);
        assert_eq!(bb.area(), 100.);
    }

    #[test]
    fn adding_two_bounding_boxes_correctly_returns_an_encompassing_bounding_box() {
        let bb1 = AABB::new(Vec2::new(-5., -5.), Vec2::new(0., 0.));
        let bb2 = AABB::new(Vec2::new(0., 0.), Vec2::new(5., 5.));
        let bb3 = bb1 + bb2;
        assert_eq!(bb3.min.x, -5.);
        assert_eq!(bb3.min.y, -5.);
        assert_eq!(bb3.max.x, 5.);
        assert_eq!(bb3.max.y, 5.);
    }

    #[test]
    fn overlapping_bounding_box() {
        let bb1 = AABB::new((-1., -1.), (1., 1.));
        let bb2 = AABB::new((0., 0.), (2., 2.));
        assert!(bb1.overlaps(&bb2));
        assert!(bb2.overlaps(&bb1));
    }

    #[test]
    fn non_overlapping_bounding_box() {
        let bb1 = AABB::new((-1., -1.), (0., 0.));
        let bb2 = AABB::new((1., 1.), (2., 2.));
        assert!(!bb1.overlaps(&bb2));
        assert!(!bb2.overlaps(&bb1));
    }
}

#[cfg(test)]
mod bvh_tests {
    use super::*;

    #[test]
    fn can_create_a_bvh() {
        let _bvh = BVH::<i32>::new();
    }

    #[test]
    fn can_insert_a_single_item_into_the_tree() {
        let mut bvh = BVH::new();
        let value = 42;
        let aabb = AABB::new((0., 0.), (1., 1.));
        bvh.insert(aabb, value);
    }

    #[test]
    fn can_insert_two_items_into_the_tree() {
        let mut bvh = BVH::new();
        bvh.insert(AABB::new((0., 0.), (1., 1.)), 42);
        bvh.insert(AABB::new((1., 1.), (2., 2.)), 7);
    }

    #[test]
    fn can_insert_three_items_into_the_tree() {
        let mut bvh = BVH::new();
        bvh.insert(AABB::new((0., 0.), (1., 1.)), 42);
        bvh.insert(AABB::new((1., 1.), (2., 2.)), 7);
        bvh.insert(AABB::new((2., 2.), (3., 3.)), 99);
    }
}
