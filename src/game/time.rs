use crate::{
    ecs::{Entity, Store, System},
};

use ngen::*;

type TimerCallback = fn(Entity, &mut Store, &Input, &Tick);

pub struct Action {
    run: f32,
    trigger: f32,
    auto_reset: bool,
    callback: TimerCallback,
}

impl Action {
    // pub fn once(t: f32, callback: TimerCallback) -> Self {
    //     Self {
    //         run: 0.,
    //         trigger: t,
    //         auto_reset: false,
    //         callback,
    //     }
    // }

    // pub fn repeat(t: f32, callback: TimerCallback) -> Self {
    //     Self {
    //         run: 0.,
    //         trigger: t,
    //         auto_reset: true,
    //         callback,
    //     }
    // }
}

pub struct Actions(Entity, Vec<Action>);

impl Actions {
    // pub const fn new(entity: Entity) -> Self {
    //     Self(entity, Vec::new())
    // }

    // pub fn actions(&self) -> &[Action] {
    //     &self.1
    // }

    pub fn actions_mut(&mut self) -> &mut [Action] {
        &mut self.1
    }

    // pub fn push_action(&mut self, action: Action) {
    //     self.1.push(action);
    // }

    fn remove(&mut self, index: usize) {
        self.1.remove(index);
    }
}

#[derive(Default)]
pub struct TimerSystem;

// TODO: Create a linked list in the arena module for better ergonomics.
struct Link<'t, T> {
    value: T,
    next: Option<&'t mut Link<'t, T>>,
}

impl<'t, T> Link<'t, T> {
    fn new(value: T, next: Option<&'t mut Link<'t, T>>) -> Self {
        Self { value, next }
    }
}

fn noop(_: Entity, _: &mut Store, _: &Input, _: &Tick) {}

impl System for TimerSystem {
    fn run(&mut self, platform: &mut Platform<'_>, store: &mut Store, input: &Input, tick: &Tick) {
        let temp_arena = &mut platform.temp_arena;

        let mut to_remove = Link::new(Entity::NULL_ENTITY, None);
        for (actions_id, actions) in store.iter::<Actions>() {
            if !store.is_live(actions.0) {
                let next = to_remove.next.take();
                let new = temp_arena.push(Link::new(actions_id, next));
                to_remove.next.replace(new);
            }
        }

        let mut next = to_remove.next.take();
        while let Some(to_remove) = next {
            store.destroy_entity(to_remove.value);
            next = to_remove.next.take();
        }

        let mut to_remove = Link::new(Entity::NULL_ENTITY, None);
        let mut callbacks =
            Link::<'_, (Entity, TimerCallback)>::new((Entity::NULL_ENTITY, noop), None);
        for (entity, action) in store.iter_mut::<Action>() {
            action.run += tick.dt;

            if action.run >= action.trigger {
                if !action.auto_reset {
                    let next = to_remove.next.take();
                    let new = temp_arena.push(Link::new(entity, next));
                    to_remove.next.replace(new);
                } else {
                    action.run -= action.trigger;
                }

                let next = callbacks.next.take();
                let new = temp_arena.push(Link::new((entity, action.callback), next));
                callbacks.next.replace(new);
            }
        }

        let mut next = to_remove.next.take();
        while let Some(to_remove) = next {
            store.remove::<Action>(to_remove.value);
            next = to_remove.next.take();
        }

        let mut actions_to_remove = Link::new(0, None);
        for (_, actions) in store.iter_mut::<Actions>() {
            let entity = actions.0;
            for (id, action) in actions.actions_mut().iter_mut().enumerate() {
                action.run += tick.dt;

                if action.run >= action.trigger {
                    if !action.auto_reset {
                        let next = actions_to_remove.next.take();
                        let new = temp_arena.push(Link::new(id, next));
                        actions_to_remove.next.replace(new);
                    } else {
                        action.run -= action.trigger;
                    }

                    let next = callbacks.next.take();
                    let new = temp_arena.push(Link::new((entity, action.callback), next));
                    callbacks.next.replace(new);
                }
            }

            // Running through the linked list will be in reverse from what we built up, allowing
            // us to call `remove` directly without having to remember offsets.
            let mut next = actions_to_remove.next.take();
            while let Some(actions_to_remove) = next {
                actions.remove(actions_to_remove.value);
                next = actions_to_remove.next.take();
            }
        }

        let mut next = callbacks.next.take();
        while let Some(to_callback) = next {
            let (entity, callback) = to_callback.value;
            callback(entity, store, input, tick);
            next = to_callback.next.take();
        }
    }
}
