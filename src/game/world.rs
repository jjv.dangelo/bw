use std::collections::HashMap;

use rand::{rngs::SmallRng, Rng, SeedableRng};

use crate::{
    ecs::{Entity, Store, System},
    game::{
        control::Control,
        physics::{Collider, Collision, RigidBody},
        player::{self, AnimationState, PlayerState},
        tiles::{
            BackgroundTile, /* CollectibleTile,*/ Environment, PlatformPiece, PlatformTile,
            Tile,
        },
        Health,
    },
};

use ngen::*;
use ngen::math::*;

pub use cgmath::prelude::*;

const CHUNK_WIDTH: f32 = crate::game::TILES_PER_X as f32 * 4.;
const CHUNK_HEIGHT: f32 = crate::game::TILES_PER_Y as f32 * 4.;
const CHUNK_WIDTH_U: u32 = CHUNK_WIDTH as u32;
// const CHUNK_HEIGHT_U: u32 = CHUNK_HEIGHT as u32;
const CHUNK_DIM: Vec2 = Vec2::new(CHUNK_WIDTH, CHUNK_HEIGHT);

fn create_tile(store: &mut Store, pos: Vec2) -> Entity {
    let tile = store.create_entity();
    let rigid_body = RigidBody::new(pos);
    store.set(tile, rigid_body);
    tile
}

fn create_env_tile(store: &mut Store, pos: Vec2, tile: BackgroundTile) -> Entity {
    let tile_id = create_tile(store, pos);
    store.set(
        tile_id,
        Tile::with_color(tile, Vec4::new(0.8, 0.8, 0.8, 0.7)),
    );
    tile_id
}

// fn create_ground_tile(store: &mut Store, pos: Vec2, piece: PlatformPiece) -> Entity {
//     let tile_id = create_tile(store, pos);
//     store.set(tile_id, Collider::new(Vec2::new(1., 1.)));
//     let tile = PlatformTile::new(Environment::Dirt, piece);
//     store.set(tile_id, Tile::new(tile));
//     tile_id
// }

// fn create_collectible_tile(store: &mut Store, pos: Vec2, tile: CollectibleTile) -> Entity {
//     let tile_id = create_tile(store, pos);
//     store.set(tile_id, Tile::with_color(tile, Vec4::new(1., 1., 1., 1.)));
//     tile_id
// }

// fn create_initial_chunk(store: &mut Store) -> WorldChunk {
//     let chunk_id = store.create_entity();
//
//     let mut chunk = {
//         let pos = Vec2::new(0., 0.);
//         WorldChunk::new(chunk_id, ChunkPos::from_world(pos))
//     };
//
//     let flowers_1 = BackgroundTile::Flower1;
//     let flowers_2 = BackgroundTile::Flower2;
//     let small_bush = BackgroundTile::Bush1;
//
//     let entities = [
//         create_ground_tile(store, Vec2::new(00., 2.), PlatformPiece::Left),
//         create_ground_tile(store, Vec2::new(01., 2.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(02., 2.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(03., 2.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(04., 2.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(05., 2.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(06., 2.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(07., 2.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(08., 2.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(09., 2.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(10., 2.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(11., 2.), PlatformPiece::Right),
//         create_ground_tile(store, Vec2::new(03., -4.), PlatformPiece::Left),
//         create_ground_tile(store, Vec2::new(04., -4.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(05., -4.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(06., -4.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(07., -4.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(08., -4.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(09., -4.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(10., -4.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(11., -4.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(12., -4.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(13., -4.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(14., -4.), PlatformPiece::Right),
//         create_ground_tile(store, Vec2::new(10., 10.), PlatformPiece::Left),
//         create_ground_tile(store, Vec2::new(11., 10.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(12., 10.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(13., 10.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(14., 10.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(15., 10.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(16., 10.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(17., 10.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(18., 10.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(19., 10.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(20., 10.), PlatformPiece::Mid),
//         create_ground_tile(store, Vec2::new(21., 10.), PlatformPiece::Right),
//         create_env_tile(store, Vec2::new(11., 11.), flowers_1),
//         create_env_tile(store, Vec2::new(12., 11.), flowers_2),
//         create_env_tile(store, Vec2::new(13., 11.), flowers_1),
//         create_env_tile(store, Vec2::new(14., 11.), small_bush),
//     ];
//
//     for entity in &entities {
//         chunk.tiles.push(*entity);
//     }
//
//     chunk
// }

fn create_player(store: &mut Store) -> Entity {
    let rigid_body = RigidBody::builder()
        .with_pos(Vec2::new(14.5, 45.5))
        .with_drag(Vec2::new(0.15, 0.15))
        .with_gravity()
        .build();

    let player = store.create_entity();

    store.set(player, PlayerState::new());
    store.set(player, rigid_body);
    store.set(player, Health(12));
    store.set(player, Collider::new(Vec2::new(1., 1.)));
    store.set(player, Vec::<Collision>::new());
    store.set(player, AnimationState::Idle);
    store.set(player, Control::new(player::player_input_callback));

    player
}

fn create_chunk(store: &mut Store, chunk_pos: ChunkPos) -> WorldChunk {
    let chunk_id = store.create_entity();
    let mut chunk = WorldChunk::new(chunk_id, chunk_pos);
    fill_chunk(store, &mut chunk);

    chunk
}

fn fill_chunk(store: &mut Store, chunk: &mut WorldChunk) {
    let mut rng = SmallRng::seed_from_u64(chunk.pos.to_seed());

    let offset = chunk.pos.inner.mul_element_wise(CHUNK_DIM);

    let flowers_1 = BackgroundTile::Flower1;
    let flowers_2 = BackgroundTile::Flower2;
    let small_bush = BackgroundTile::Bush1;

    for _ in 0..rng.gen_range(0, 2) {
        chunk.add_platform(store, offset + Vec2::new(3., 0.), CHUNK_WIDTH_U / 4);
        for x in 6..CHUNK_WIDTH_U / 4 - 1 {
            let env = rng.gen_range(0., 1.);
            if env >= 0. && env < 0.15 {
                create_env_tile(store, offset + Vec2::new(x as f32, 1.), flowers_1);
            } else if env >= 0.15 && env < 0.30 {
                create_env_tile(store, offset + Vec2::new(x as f32, 1.), flowers_2);
            } else if env >= 0.30 && env < 0.45 {
                create_env_tile(store, offset + Vec2::new(x as f32, 1.), small_bush);
            }
        }

        chunk.add_platform(store, offset + Vec2::new(4., 1.), 1);
        let env = rng.gen_range(0., 1.);
        if env >= 0. && env < 0.15 {
            create_env_tile(store, offset + Vec2::new(4., 2.), flowers_1);
        } else if env >= 0.15 && env < 0.30 {
            create_env_tile(store, offset + Vec2::new(4., 2.), flowers_2);
        } else if env >= 0.30 && env < 0.45 {
            create_env_tile(store, offset + Vec2::new(4., 2.), small_bush);
        }

        chunk.add_platform(store, offset + Vec2::new(5., 2.), 1);
        let env = rng.gen_range(0., 1.);
        if env >= 0. && env < 0.15 {
            create_env_tile(store, offset + Vec2::new(5., 3.), flowers_1);
        } else if env >= 0.15 && env < 0.30 {
            create_env_tile(store, offset + Vec2::new(5., 3.), flowers_2);
        } else if env >= 0.30 && env < 0.45 {
            create_env_tile(store, offset + Vec2::new(5., 3.), small_bush);
        }

        chunk.add_platform(store, offset + Vec2::new(6., 3.), 3);
        let env = rng.gen_range(0., 1.);
        if env >= 0. && env < 0.15 {
            create_env_tile(store, offset + Vec2::new(6., 4.), flowers_1);
        } else if env >= 0.15 && env < 0.30 {
            create_env_tile(store, offset + Vec2::new(6., 4.), flowers_2);
        } else if env >= 0.30 && env < 0.45 {
            create_env_tile(store, offset + Vec2::new(6., 4.), small_bush);
        }
    }
}

fn create_initial_camera(store: &mut Store) -> Entity {
    let camera_id = store.create_entity();

    let camera_tiles_per_x = crate::game::TILES_PER_X as f32;
    let screen_width = crate::game::SCREEN_WIDTH as f32;
    let screen_height = crate::game::SCREEN_HEIGHT as f32;

    let r = screen_height / screen_width;
    let n = camera_tiles_per_x * 0.5;
    let d = n * r / (std::f32::consts::FRAC_PI_4).tan();
    let h = n * r;

    let camera = {
        let camera_eye = P3::new(n, h, d);
        let camera_target = P3::new(n, h, 0.);
        Camera::new(camera_eye, camera_target, screen_width / screen_height)
    };
    store.set(camera_id, camera);

    camera_id
}

struct WorldChunk {
    id: Entity,
    pos: ChunkPos,
    tiles: Vec<Entity>,
}

impl WorldChunk {
    fn new(id: Entity, pos: ChunkPos) -> Self {
        Self {
            id,
            pos,
            tiles: Vec::new(),
        }
    }

    fn destroy(&mut self, store: &mut Store) {
        store.destroy_entity(self.id);

        for id in self.tiles.drain(0..) {
            store.destroy_entity(id);
        }
    }

    fn add_ground_tile(&mut self, store: &mut Store, pos: Vec2, tile: PlatformPiece) {
        let tile_id = create_tile(store, pos);
        store.set(tile_id, Collider::new(Vec2::new(1., 1.)));
        let tile = PlatformTile::new(Environment::Dirt, tile);
        store.set(tile_id, Tile::new(tile));

        self.tiles.push(tile_id);
    }

    fn add_platform(&mut self, store: &mut Store, mut pos: Vec2, length: u32) {
        if length == 0 {
            return;
        }

        if length == 1 {
            self.add_ground_tile(store, pos, PlatformPiece::Single);

            return;
        }

        self.add_ground_tile(store, pos, PlatformPiece::Left);

        for _ in 1..length - 1 {
            pos.x += 1.;
            self.add_ground_tile(store, pos, PlatformPiece::Mid);
        }

        pos.x += 1.;
        self.add_ground_tile(store, pos, PlatformPiece::Right);
    }
}

#[derive(Clone, Copy)]
struct ChunkPos {
    inner: Vec2,
}

impl ChunkPos {
    const fn new(inner: Vec2) -> Self {
        Self { inner }
    }

    fn from_world(Vec2 { x, y }: Vec2) -> Self {
        Self::new(Vec2::new(
            x.div_euclid(CHUNK_WIDTH),
            y.div_euclid(CHUNK_HEIGHT),
        ))
    }

    fn to_seed(&self) -> u64 {
        let x = (self.inner.x.to_bits() as u64) << 32;
        let y = self.inner.y.to_bits() as u64;

        x | y
    }
}

impl PartialEq for ChunkPos {
    fn eq(&self, other: &Self) -> bool {
        self.inner == other.inner
    }
}

impl std::hash::Hash for ChunkPos {
    fn hash<H: std::hash::Hasher>(&self, hasher: &mut H) {
        let x = self.inner.x.to_bits();
        let y = self.inner.y.to_bits();

        x.hash(hasher);
        y.hash(hasher);
    }
}

impl Eq for ChunkPos {}

pub struct WorldSystem {
    player_id: Entity,
    camera_id: Entity,
    chunks: HashMap<ChunkPos, WorldChunk>,
}

impl WorldSystem {
    pub fn new(store: &mut Store) -> Self {
        let player_id = create_player(store);
        let camera_id = create_initial_camera(store);

        let chunks = HashMap::with_capacity(9);
        // let chunk = create_initial_chunk(store);
        // chunks.insert(ChunkPos::from_world(Vec2::new(0., 0.)), chunk);

        Self {
            player_id,
            camera_id,
            chunks,
        }
    }
}

impl System for WorldSystem {
    fn run(&mut self, platform: &mut Platform<'_>, store: &mut Store, input: &Input, tick: &Tick) {
        let Self {
            player_id,
            camera_id,
            chunks,
        } = self;

        if let Some(player_pos) = store.get::<RigidBody>(*player_id).map(|rb| rb.pos) {
            let mut to_delete = platform.temp_arena.array();

            if let Some(camera) = store.get_mut::<Camera>(*camera_id) {
                let Vec2 { x, y } =
                    Vec2::new(camera.look_at.x, camera.look_at.y).lerp(player_pos, tick.dt * 3.);

                camera.look_at.x = x;
                camera.look_at.y = y;

                camera.eye.x = x;
                camera.eye.y = y;

                if input.keyboard.f[09].is_down() {
                    camera.eye.z += 1.;
                }

                if input.keyboard.f[10].is_down() {
                    camera.eye.z -= 1.;
                }
            }

            let player_chunk_pos = ChunkPos::from_world(player_pos);

            // TODO: Re-use the expired chunks so we avoid deallocations and allocations.
            for chunk_pos in chunks.keys() {
                let dist = chunk_pos.inner.distance2(player_chunk_pos.inner);

                if dist > 4. {
                    to_delete.push(*chunk_pos);
                }
            }

            for chunk_pos in to_delete.end() {
                if let Some(mut chunk) = chunks.remove(chunk_pos) {
                    chunk.destroy(store);
                }
            }

            let offsets = [
                Vec2::new(0., 0.),
                Vec2::new(-1., -1.),
                Vec2::new(-1., 1.),
                Vec2::new(-1., 0.),
                Vec2::new(0., 1.),
                Vec2::new(1., 1.),
                Vec2::new(1., -1.),
                Vec2::new(1., 0.),
                Vec2::new(0., -1.),
            ];

            for offset in &offsets {
                let world_pos = ChunkPos::new(player_chunk_pos.inner + offset);

                chunks
                    .entry(world_pos)
                    .or_insert_with(|| create_chunk(store, world_pos));
            }
        }

        if input.keyboard.f[12].was_down() {
            println!("tick: {}", tick.total_time);
            println!("\t chunk_count: {}", chunks.len());
            println!(
                "\t platform_tiles: {}",
                store.iter::<Tile<PlatformTile>>().count()
            );
            println!("\t entity_count: {}", store.entity_count());
            println!();
        }
    }
}
