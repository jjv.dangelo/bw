use crate::{
    ecs::{Entity, Store, System},
};

use ngen::*;

pub type ControlCallback = fn(Entity, &mut Store, &Input, &Tick);
pub struct Control(ControlCallback);

impl Control {
    pub fn new(callback: ControlCallback) -> Self {
        Self(callback)
    }
}

#[derive(Default)]
pub struct ControlSystem;

impl System for ControlSystem {
    fn run(&mut self, platform: &mut Platform<'_>, store: &mut Store, input: &Input, tick: &Tick) {
        let mut callbacks = platform.temp_arena.array();

        for (entity, Control(callback)) in store.iter() {
            callbacks.push((entity, *callback));
        }

        for (entity, callback) in callbacks.end() {
            callback(*entity, store, input, tick);
        }
    }
}
