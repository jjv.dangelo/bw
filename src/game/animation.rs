#![allow(dead_code)]

use crate::{
    ecs::{Store, System},
};

use ngen::*;
use ngen::assets::Sprite;

struct Frame {
    bitmap: Sprite,
    delay: f32,
}

impl Frame {
    const fn new(bitmap: Sprite, delay: f32) -> Self {
        Self { bitmap, delay }
    }
}

pub struct Animation {
    frame: usize,
    running: f32,
    frames: Vec<Frame>,
    paused: bool,
}

impl Animation {
    pub fn new() -> Self {
        Self {
            frame: 0,
            running: 0.,
            frames: Vec::new(),
            paused: false,
        }
    }

    pub fn push_bitmap(&mut self, bitmap: Sprite, delay: f32) {
        self.frames.push(Frame::new(bitmap, delay));
    }

    pub fn get_sprite(&self) -> Sprite {
        self.frames[self.frame].bitmap
    }

    pub fn advance(&mut self, dt: f32) -> bool {
        if !self.paused {
            self.running += dt;
            let delay = self.frames[self.frame].delay;

            if self.running >= delay {
                self.running -= delay;
                self.frame = (self.frame + 1) % self.frames.len()
            }
        }

        !self.paused
    }

    pub fn pause(&mut self) {
        self.paused = true;
    }

    pub fn unpause(&mut self) {
        self.paused = false;
    }
}

#[derive(Default)]
pub struct AnimationSystem;

impl System for AnimationSystem {
    fn run(&mut self, _: &mut Platform, store: &mut Store, _: &Input, tick: &Tick) {
        for (_, animation, sprite) in store.view_mut::<(Animation, Sprite)>() {
            if animation.advance(tick.dt) {
                *sprite = animation.get_sprite();
            }
        }
    }
}
