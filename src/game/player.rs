#![allow(dead_code)]

use crate::{
    ecs::{Entity, Store, System},
    game::{
        animation::Animation,
        physics::{Collision, RigidBody},
    },
};

use ngen::*;
use ngen::assets::TiledBitmap;

#[derive(Clone, Copy)]
pub struct State<S> {
    inner: S,
}

impl<S> State<S> {
    const fn wrap(inner: S) -> Self {
        Self { inner }
    }
}

impl<S> std::ops::Deref for State<S> {
    type Target = S;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<S> std::ops::DerefMut for State<S> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

#[derive(Clone, Copy)]
pub struct Grounded {
    can_jump: bool,
    last_input: f32,
}

impl Grounded {
    const fn new() -> Self {
        Self {
            can_jump: false,
            last_input: 0.,
        }
    }
}

#[derive(Clone, Copy)]
pub struct Jumping {
    jump_boost: Option<f32>,
}

pub enum PlayerState {
    Grounded(State<Grounded>),
    Jumping(State<Jumping>),
}

impl PlayerState {
    pub fn new() -> Self {
        Self::Grounded(State::new())
    }
}

impl State<Grounded> {
    pub const fn new() -> Self {
        State::wrap(Grounded::new())
    }

    fn jump(self) -> State<Jumping> {
        self.into()
    }
}

impl State<Jumping> {
    fn ground(self) -> State<Grounded> {
        self.into()
    }
}

impl Into<State<Jumping>> for State<Grounded> {
    fn into(self) -> State<Jumping> {
        State::wrap(Jumping {
            jump_boost: Some(0.),
        })
    }
}

impl Into<State<Grounded>> for State<Jumping> {
    fn into(self) -> State<Grounded> {
        State::wrap(Grounded::new())
    }
}

pub fn player_input_callback(_: Entity, store: &mut Store, input: &Input, _: &Tick) {
    for (_, state, rigid_body) in store.view_mut::<(PlayerState, RigidBody)>() {
        let left = input.controller.left.is_down() as u32 as f32;
        let right = input.controller.right.is_down() as u32 as f32;
        let down = input.controller.down.is_down() as u32 as f32;
        let up = input.controller.up.is_down() as u32 as f32;

        rigid_body.vel.x -= left * 3.;
        rigid_body.vel.x += right * 3.;

        if let PlayerState::Grounded(ref mut g) = state {
            if rigid_body.gravity && g.can_jump && input.controller.select.is_down() {
                *state = PlayerState::Jumping(g.jump());
            } else if !rigid_body.gravity {
                rigid_body.vel.y -= down * 3.;
                rigid_body.vel.y += up * 3.;
            }
        }

        if let PlayerState::Jumping(ref mut j) = state {
            if let Some(i) = j.jump_boost.take() {
                if input.controller.select.is_down() {
                    let w = 0.25;
                    let jump_strength = 60.;

                    let c = (w * i).cos();
                    if c >= 0. {
                        j.jump_boost.replace(i + 1.);
                        rigid_body.vel.y += c * jump_strength
                    }
                }
            }
        }
    }
}

#[derive(Default)]
pub struct PlayerCollisionSystem;

impl System for PlayerCollisionSystem {
    fn run(&mut self, _: &mut Platform<'_>, store: &mut Store, input: &Input, _: &Tick) {
        for (_, state, _, collisions) in
            store.view_mut::<(PlayerState, RigidBody, Vec<Collision>)>()
        {
            let contact_with_ground = collisions.iter().any(|c| c.cn.y == 1.);

            if let PlayerState::Jumping(ref mut j) = state {
                if contact_with_ground {
                    *state = PlayerState::Grounded(j.ground());
                }
            }

            if let PlayerState::Grounded(ref mut g) = state {
                if !contact_with_ground {
                    let mut jump_state = g.jump();
                    jump_state.jump_boost = None;
                    *state = PlayerState::Jumping(jump_state);
                } else if contact_with_ground && !g.can_jump {
                    g.can_jump = !input.controller.select.is_down();
                }
            }
        }
    }
}

pub enum AnimationState {
    Idle,
    Running,
    Jumping,
}

pub struct PlayerAnimationSystem {
    spritesheet: TiledBitmap,
}

impl PlayerAnimationSystem {
    pub const fn new(spritesheet: TiledBitmap) -> Self {
        Self { spritesheet }
    }
}

impl System for PlayerAnimationSystem {
    fn run(&mut self, platform: &mut Platform<'_>, store: &mut Store, _: &Input, _: &Tick) {
        let temp_arena = &mut platform.temp_arena;
        let mut changes = temp_arena.array();

        for (entity, state, rigid_body, anim_state) in
            store.view_mut::<(PlayerState, RigidBody, AnimationState)>()
        {
            match state {
                PlayerState::Jumping(_) => match anim_state {
                    AnimationState::Idle | AnimationState::Running => {
                        *anim_state = AnimationState::Jumping;
                        changes.push((entity, AnimationState::Jumping));
                    }

                    AnimationState::Jumping => {}
                },

                PlayerState::Grounded(_) => match anim_state {
                    AnimationState::Jumping if rigid_body.vel.x.abs() > 0.1 => {
                        *anim_state = AnimationState::Running;
                        changes.push((entity, AnimationState::Running));
                    }

                    AnimationState::Idle if rigid_body.vel.x.abs() > 0.1 => {
                        *anim_state = AnimationState::Running;
                        changes.push((entity, AnimationState::Running));
                    }

                    AnimationState::Running if rigid_body.vel.x.abs() <= 0.1 => {
                        *anim_state = AnimationState::Idle;
                        changes.push((entity, AnimationState::Idle));
                    }

                    AnimationState::Jumping => {
                        *anim_state = AnimationState::Idle;
                        changes.push((entity, AnimationState::Idle));
                    }

                    AnimationState::Idle | AnimationState::Running => (),
                },
            }
        }

        for (entity, anim_state) in changes.end() {
            let spritesheet = &self.spritesheet;

            let animation = match anim_state {
                AnimationState::Jumping => {
                    let mut animation = Animation::new();
                    let frame_1 = spritesheet.get_sprite(4, 12);
                    animation.push_bitmap(frame_1, 0.15);

                    animation
                }

                AnimationState::Running => {
                    let mut animation = Animation::new();
                    let frame_1 = spritesheet.get_sprite(1, 12);
                    let frame_2 = spritesheet.get_sprite(2, 12);
                    let frame_3 = spritesheet.get_sprite(3, 12);
                    animation.push_bitmap(frame_1, 0.15);
                    animation.push_bitmap(frame_2, 0.15);
                    animation.push_bitmap(frame_3, 0.15);

                    animation
                }

                AnimationState::Idle => {
                    let mut animation = Animation::new();
                    let frame_1 = spritesheet.get_sprite(0, 12);
                    animation.push_bitmap(frame_1, 0.15);

                    animation
                }
            };

            store.set(*entity, animation.get_sprite());
            store.set(*entity, animation);
        }
    }
}
