#![allow(dead_code)]

use crate::{
    math::*,
};

use ngen::assets::{Sprite, TiledBitmap};

pub trait Tiled {
    fn get_sprite(&self, spritesheet: &TiledBitmap) -> Sprite;
}

#[derive(Clone, Copy)]
pub enum Environment {
    Space,
    Dirt,
    Mountain,
    Mechanical,
}

#[derive(Clone, Copy)]
pub enum PlatformPiece {
    Single,
    Left,
    Mid,
    Right,
}

#[derive(Clone, Copy)]
pub struct PlatformTile {
    pub env: Environment,
    pub piece: PlatformPiece,
}

impl PlatformTile {
    pub const fn new(env: Environment, piece: PlatformPiece) -> Self {
        Self { env, piece }
    }

    const fn get_sprite_coords(&self) -> (u32, u32) {
        let x = match self.piece {
            PlatformPiece::Single => 3,
            PlatformPiece::Left => 4,
            PlatformPiece::Mid => 5,
            PlatformPiece::Right => 6,
        };

        let y = match self.env {
            Environment::Space => 3,
            Environment::Dirt => 4,
            Environment::Mountain => 5,
            Environment::Mechanical => 6,
        };

        (x, y)
    }
}

impl Tiled for PlatformTile {
    fn get_sprite(&self, spritesheet: &TiledBitmap) -> Sprite {
        let (x, y) = self.get_sprite_coords();
        spritesheet.get_sprite(x, y)
    }
}

pub struct Tile<T> {
    pub tile: T,
    pub color: Vec4,
}

impl<T: Clone> Clone for Tile<T> {
    fn clone(&self) -> Self {
        Self {
            tile: self.tile.clone(),
            color: self.color,
        }
    }
}

impl<T: Copy> Copy for Tile<T> {}

impl<T: Tiled> Tile<T> {
    pub const fn new(tile: T) -> Self {
        Self::with_color(tile, Vec4::new(1., 1., 1., 1.))
    }

    pub const fn with_color(tile: T, color: Vec4) -> Self {
        Self { tile, color }
    }
}

impl<T: Tiled> Tiled for Tile<T> {
    fn get_sprite(&self, spritesheet: &TiledBitmap) -> Sprite {
        self.tile.get_sprite(spritesheet)
    }
}

#[derive(Clone, Copy)]
pub enum BackgroundTile {
    Flower1,
    Flower2,
    Grass1,
    Grass2,
    Bush1,
}

impl Tiled for BackgroundTile {
    fn get_sprite(&self, spritesheet: &TiledBitmap) -> Sprite {
        match self {
            Self::Flower1 => spritesheet.get_sprite(16, 0),
            Self::Flower2 => spritesheet.get_sprite(17, 0),
            Self::Grass1 => spritesheet.get_sprite(18, 0),
            Self::Grass2 => spritesheet.get_sprite(18, 1),
            Self::Bush1 => spritesheet.get_sprite(13, 0),
        }
    }
}

#[derive(Clone, Copy)]
pub enum CollectibleTile {
    Heart1,
    Heart2,
    Heart3,
}

impl Tiled for CollectibleTile {
    fn get_sprite(&self, spritesheet: &TiledBitmap) -> Sprite {
        match self {
            Self::Heart1 => spritesheet.get_sprite(0, 2),
            Self::Heart2 => spritesheet.get_sprite(1, 2),
            Self::Heart3 => spritesheet.get_sprite(2, 2),
        }
    }
}
