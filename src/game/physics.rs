#![allow(dead_code)]

pub mod bvh;

use std::collections::HashMap;

use crate::{
    ecs::{Entity, Store, System, Systems},
    math::Ray,
};

use bvh::{AABB, BVH};

pub use cgmath::prelude::*;

use ngen::*;
use ngen::math::*;

pub struct RigidBody {
    pub pos: Vec2,
    pub vel: Vec2,
    pub drag: Vec2,
    pub gravity: bool,
    pub forces: Vec<Vec2>,
}

pub struct RigidBodyBuilder {
    rb: RigidBody,
}

impl RigidBodyBuilder {
    pub fn with_pos(mut self, pos: Vec2) -> Self {
        self.rb.pos = pos;
        self
    }

    pub fn with_drag(mut self, drag: Vec2) -> Self {
        self.rb.drag = drag;
        self
    }

    pub fn with_gravity(mut self) -> Self {
        self.rb.gravity = true;
        self
    }

    pub fn build(self) -> RigidBody {
        self.rb
    }
}

impl RigidBody {
    pub const fn new(pos: Vec2) -> Self {
        Self {
            pos,
            vel: Vec2::new(0., 0.),
            drag: Vec2::new(0., 0.),
            gravity: false,
            forces: Vec::new(),
        }
    }

    pub fn builder() -> RigidBodyBuilder {
        RigidBodyBuilder {
            rb: RigidBody::new(Vec2::new(0., 0.)),
        }
    }
}

pub type CollisionTrigger = fn(&Collision, &mut Store, input: &Input, tick: &Tick);

#[derive(Clone, Copy)]
pub struct Collider {
    pub size: Vec2,
    pub offset: Vec2,
    pub trigger: Option<CollisionTrigger>,
}

impl Collider {
    pub fn new(size: Vec2) -> Self {
        Self {
            size,
            offset: Vec2::new(0., 0.),
            trigger: None,
        }
    }

    pub fn new_trigger(size: Vec2, trigger: CollisionTrigger) -> Self {
        Self {
            size,
            offset: Vec2::new(0., 0.),
            trigger: Some(trigger),
        }
    }

    pub fn set_trigger_callback(&mut self, trigger: CollisionTrigger) {
        self.trigger = Some(trigger);
    }

    pub fn remove_trigger_callback(&mut self) {
        self.trigger = None;
    }
}

pub struct PhysicsSystem {
    systems: Systems,
}

impl PhysicsSystem {
    pub fn new() -> Self {
        let mut systems = Systems::new();

        systems.register_default::<GravitySystem>();
        systems.register_default::<CollisionDetectionSystem>();
        systems.register_default::<ForcesSystem>();

        Self { systems }
    }
}

impl System for PhysicsSystem {
    fn run(&mut self, platform: &mut Platform<'_>, store: &mut Store, input: &Input, tick: &Tick) {
        self.systems.run(platform, store, input, tick);
    }
}

#[derive(Default)]
pub struct GravitySystem;

impl GravitySystem {
    pub const GRAVITY: Vec2 = Vec2::new(0., -7.182);
}

impl System for GravitySystem {
    fn run(&mut self, _: &mut Platform<'_>, store: &mut Store, _: &Input, _: &Tick) {
        for (_, rigid_body) in store.iter_mut::<RigidBody>() {
            if rigid_body.gravity {
                rigid_body.vel += Self::GRAVITY;
            }

            for force in rigid_body.forces.drain(0..) {
                rigid_body.vel += force;
            }
        }
    }
}

#[derive(Default)]
struct ForcesSystem;

impl System for ForcesSystem {
    fn run(&mut self, _: &mut Platform<'_>, store: &mut Store, _: &Input, tick: &Tick) {
        for (_, rigid_body) in store.iter_mut::<RigidBody>() {
            rigid_body.pos += rigid_body.vel * tick.dt;
            rigid_body.vel -= rigid_body.vel.mul_element_wise(rigid_body.drag);

            if rigid_body.vel.magnitude2() < 0.1 {
                rigid_body.vel = Vec2::new(0., 0.);
            }
        }
    }
}

pub struct Collision {
    pub entity: Entity,
    pub entity_pos: Vec2,

    pub other: Entity,
    pub other_pos: Vec2,
    pub cn: Vec2,
    pub t: f32,

    pub is_physical: bool,
}

impl Collision {
    pub fn distance(&self) -> f32 {
        self.entity_pos.distance(self.other_pos)
    }

    pub fn distance2(&self) -> f32 {
        self.entity_pos.distance2(self.other_pos)
    }
}

// TODO: Use the temp_arena passed in instead of hanging on to the Vecs and HashMaps.
#[derive(Default)]
struct CollisionDetectionSystem {
    x_collisions: Vec<Collision>,
    y_collisions: Vec<Collision>,
    collisions: HashMap<Entity, Vec<Collision>>,
}

impl System for CollisionDetectionSystem {
    fn run(&mut self, _: &mut Platform<'_>, store: &mut Store, input: &Input, tick: &Tick) {
        let x_collisions = &mut self.x_collisions;
        let y_collisions = &mut self.y_collisions;
        let collisions = &mut self.collisions;

        let bvh = store
            .view::<(RigidBody, Collider)>()
            .map(|(e, rb, c)| {
                (
                    AABB::from_pos(rb.pos + c.offset, c.size),
                    (e, rb.pos, c.size, c.trigger.is_none()),
                )
            })
            .collect::<BVH<(Entity, Vec2, Vec2, bool)>>();

        for (_, collisions) in store.iter_mut::<Vec<Collision>>() {
            collisions.clear();
        }

        for (entity, rb) in store
            .iter_mut::<RigidBody>()
            .filter(|(_, rb)| rb.vel.magnitude2() != 0.)
        {
            let frame_vel = rb.vel * tick.dt;

            // Look for collisions in the x-direction and then the y-direction, instead of the
            // direction of the current velocity. This way we can resolve the collisions without
            // getting stuck on corners of ground tiles.

            let x_ray_dir = Vec2::new(rb.vel.x, 0.).normalize();
            let x_ray = Ray::new(rb.pos, x_ray_dir);
            for ((other, other_pos, dim, is_physical), mut t) in bvh
                .cast_ray(x_ray, Some(Vec2::new(1., 1.)))
                .filter(|(e, _)| e.0 != entity)
            {
                t /= frame_vel.x.abs();
                if t < 0. || t >= 1. {
                    continue;
                }

                let cn = {
                    let contact_pos = rb.pos + Vec2::new(frame_vel.x, 0.);
                    let from_center = contact_pos - other_pos;
                    let dim_ratio = from_center.div_element_wise(dim * 0.5);
                    let divisor = dim_ratio.x.abs().max(dim_ratio.y.abs());
                    let normal = dim_ratio / divisor;

                    Vec2::new(normal.x as i32 as f32, normal.y as i32 as f32)
                };

                x_collisions.push(Collision {
                    entity,
                    entity_pos: rb.pos,
                    other: *other,
                    other_pos: *other_pos,
                    cn,
                    t,
                    is_physical: *is_physical,
                });
            }

            let y_ray_dir = Vec2::new(0., rb.vel.y).normalize();
            let y_ray = Ray::new(rb.pos, y_ray_dir);
            for ((other, other_pos, dim, is_physical), mut t) in bvh
                .cast_ray(y_ray, Some(Vec2::new(1., 1.)))
                .filter(|(e, _)| e.0 != entity)
            {
                t /= frame_vel.y.abs();
                if t < 0. || t >= 1. {
                    continue;
                }

                let cn = {
                    let contact_pos = rb.pos + Vec2::new(0., frame_vel.y);
                    let from_center = contact_pos - other_pos;
                    let dim_ratio = from_center.div_element_wise(dim * 0.5);
                    let divisor = dim_ratio.x.abs().max(dim_ratio.y.abs());
                    let normal = dim_ratio / divisor;

                    Vec2::new(normal.x as i32 as f32, normal.y as i32 as f32)
                };

                y_collisions.push(Collision {
                    entity,
                    entity_pos: rb.pos,
                    other: *other,
                    other_pos: *other_pos,
                    cn,
                    t,
                    is_physical: *is_physical,
                });
            }

            if x_collisions.len() == 0 && y_collisions.len() == 0 {
                continue;
            }

            x_collisions.sort_by(|l, r| {
                let l_dist = l.other_pos.distance(rb.pos);
                let r_dist = r.other_pos.distance(rb.pos);

                l_dist.total_cmp(&r_dist)
            });

            y_collisions.sort_by(|l, r| {
                let l_dist = l.other_pos.distance(rb.pos);
                let r_dist = r.other_pos.distance(rb.pos);

                l_dist.total_cmp(&r_dist)
            });

            if let Some(collision) = x_collisions.iter().filter(|c| c.is_physical).next() {
                let offset = Vec2::new(-frame_vel.x, 0.) * (1. - collision.t);
                rb.pos += offset;
            }

            if let Some(collision) = y_collisions.iter().filter(|c| c.is_physical).next() {
                let offset = Vec2::new(0., -frame_vel.y) * (1. - collision.t);
                rb.pos += offset;
            }

            let entity_collisions = collisions
                .entry(entity)
                .or_insert(Vec::with_capacity(x_collisions.len() + y_collisions.len()));

            entity_collisions.append(y_collisions);
            entity_collisions.append(x_collisions);
        }

        for (entity, collisions) in collisions.drain() {
            for collision in collisions.iter() {
                if let Some(trigger) = store
                    .get::<Collider>(collision.other)
                    .and_then(|c| c.trigger)
                {
                    trigger(collision, store, input, tick);
                }
            }

            store.set(entity, collisions);
        }
    }
}
