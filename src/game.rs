mod animation;
mod control;
mod physics;
mod player;
mod tiles;
mod time;
mod world;

use crate::{
    ecs::{Store, Systems},
    game::{
        animation::AnimationSystem,
        control::ControlSystem,
        physics::{PhysicsSystem, RigidBody},
        player::{PlayerAnimationSystem, PlayerCollisionSystem},
        tiles::{BackgroundTile, CollectibleTile, PlatformTile, Tile, Tiled},
        time::TimerSystem,
        world::WorldSystem,
    },
};

pub use cgmath::prelude::*;

use ngen::*;
use ngen::math::*;
use ngen::assets::*;

const TILE_SIZE: usize = 30;
const SCREEN_WIDTH: usize = 1920;
const SCREEN_HEIGHT: usize = 1080;
const TILES_PER_X: usize = SCREEN_WIDTH / TILE_SIZE;
const TILES_PER_Y: usize = SCREEN_HEIGHT / TILE_SIZE;

struct Health(i32);

pub struct Game {
    store: Store,
    systems: Systems,
    spritesheet: TiledBitmap,
    _effects: TiledBitmap,
}

impl Game {
    pub fn new(platform: &mut Platform<'_>) -> Result<Self, String> {
        let spritesheet = {
            let image = platform.load_image("assets/monochrome_tilemap_transparent_packed.png")?;
            TiledBitmap::new(image, 20, 20)
        };

        let _effects = {
            let image = platform.load_image("assets/effects/spritesheet.png")?;
            TiledBitmap::new(image, 9, 9)
        };

        let mut store = Store::new();
        let mut systems = Systems::new();

        systems.register(WorldSystem::new(&mut store));
        systems.register_default::<TimerSystem>();
        systems.register_default::<ControlSystem>();
        systems.register(PhysicsSystem::new());
        systems.register_default::<PlayerCollisionSystem>();
        systems.register(PlayerAnimationSystem::new(spritesheet));
        systems.register_default::<AnimationSystem>();

        Ok(Self {
            store,
            systems,
            spritesheet,
            _effects,
        })
    }
}

impl ngen::Simulation for Game {
    fn update(
        &mut self,
        platform: &mut ngen::Platform<'_>,
        input: &ngen::Input,
        tick: Tick,
    ) -> Result<bool, String> {
        if input.keyboard.escape.was_down() {
            return Ok(false);
        }

        for (_, Health(ref mut health)) in self.store.iter_mut::<Health>() {
            *health -= input.keyboard.f[01].was_down() as i32;
            *health += input.keyboard.f[02].was_down() as i32;
        }

        let systems = &mut self.systems;
        let store = &mut self.store;
        systems.run(platform, store, input, &tick);

        Ok(true)
    }

    fn render(&mut self, render_commands: &mut RenderCommands) -> Result<(), String> {
        let tiles_per_x = TILES_PER_X as f32;
        let screen_width = SCREEN_WIDTH as f32;
        let screen_height = SCREEN_HEIGHT as f32;
        let r = screen_height / screen_width;
        let n = tiles_per_x * 0.5;
        let d = n * r / (std::f32::consts::FRAC_PI_4).tan();

        let (camera_eye, transform) = self
            .store
            .iter::<Camera>()
            .next()
            .map(|(_, c)| (c.eye, c.build_transform())).ok_or("Couldn't find a camera".to_string())?;

        let ui_camera_transform = {
            let camera_eye = P3::new(0., 0., d);
            let camera_target = P3::new(0., 0., 0.);
            let camera = Camera::new(camera_eye, camera_target, screen_width / screen_height);
            camera.build_transform()
        };

        let white = Vec4::new(1., 1., 1., 1.);
        let black = Vec4::new(0., 0., 0., 0.);

        render_commands.with_render_layer(transform, |layer| {
            layer.clear_screen(black);

            for (_, rigid_body, platform_tile) in
                self.store.view::<(RigidBody, Tile<BackgroundTile>)>()
            {
                let sprite = platform_tile.get_sprite(&self.spritesheet);
                layer.push_sprite(
                    rigid_body.pos,
                    Vec2::new(1., 1.),
                    platform_tile.color,
                    sprite,
                );
            }
        });

        render_commands.with_render_layer(transform, |layer| {
            for (_, rigid_body, platform_tile) in
                self.store.view::<(RigidBody, Tile<CollectibleTile>)>()
            {
                let sprite = platform_tile.get_sprite(&self.spritesheet);
                layer.push_sprite(
                    rigid_body.pos,
                    Vec2::new(1., 1.),
                    platform_tile.color,
                    sprite,
                );
            }
        });

        render_commands.with_render_layer(transform, |layer| {
            use physics::bvh::{AABB, BVH};

            let bvh = self
                .store
                .view::<(RigidBody, Tile<PlatformTile>)>()
                .map(|(_, rb, tile)| (AABB::from_pos(rb.pos, Vec2::new(1., 1.)), (rb.pos, tile)))
                .collect::<BVH<(Vec2, &Tile<PlatformTile>)>>();

            let scene_aabb = AABB::from_pos(
                Vec2::new(camera_eye.x, camera_eye.y),
                Vec2::new(TILES_PER_X as f32, TILES_PER_Y as f32),
            );

            for (pos, tile) in bvh.cast_aabb(scene_aabb) {
                let sprite = tile.get_sprite(&self.spritesheet);
                layer.push_sprite(*pos, Vec2::new(1., 1.), tile.color, sprite);
            }
        });

        render_commands.with_render_layer(transform, |layer| {
            for (_, rigid_body, sprite) in self.store.view::<(RigidBody, Sprite)>() {
                if rigid_body.vel.magnitude2() != 0. {
                    let mut sprite = *sprite;

                    if rigid_body.vel.x < 0. {
                        sprite = sprite.flip_uv_x();
                    }

                    layer.push_sprite(rigid_body.pos, Vec2::new(1., 1.), white, sprite);
                } else {
                    layer.push_sprite(rigid_body.pos, Vec2::new(1., 1.), white, *sprite);
                }
            }
        });

        render_commands.with_render_layer(ui_camera_transform, |layer| {
            let mut pos = Vec2::new(-31., 17.);
            let size = Vec2::new(1., 1.);

            if let Some((_, Health(mut health))) = self.store.iter::<Health>().next() {
                let wrap_x = pos.x + 8.;

                while health > 0 {
                    health -= 3;

                    let sprite = match health {
                        -2 => self.spritesheet.get_sprite(0, 2),
                        -1 => self.spritesheet.get_sprite(1, 2),
                        _ if health >= 0 => self.spritesheet.get_sprite(2, 2),
                        _ => break,
                    };

                    layer.push_sprite(pos, size, white, sprite);

                    pos.x += 1.;
                    if pos.x == wrap_x {
                        pos.x = -31.;
                        pos.y -= 1.;
                    }
                }
            }
        });

        Ok(())
    }
}
