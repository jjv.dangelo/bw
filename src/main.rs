#![feature(box_syntax, const_fn, total_cmp)]

mod ecs;
mod game;
mod math;

fn main() -> Result<(), String> {
    ngen::builder()
        .window(1920, 1080, "bw")
        .max_quads(25_000)
        .temp_memory(1024 * 1024)
        .mount_init(|platform| {
            game::Game::new(platform).unwrap()
        })
}
